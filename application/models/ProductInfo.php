<?php

class ProductInfo extends Eloquent {

    // table名
    public static $table = "product_infos";

    /**
     * 货品说明&货品 (多对一)
     */
    public function product()
    {
        return $this->belongs_to('Product');
    }

}

?>
