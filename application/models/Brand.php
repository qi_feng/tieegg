<?php

class Brand extends Eloquent {

    // table名
    public static $table = "brands";

    /**
     * 品牌&货品(一对多)
     */
    public function products()
    {
        return $this->has_many('Product');
    }

}

?>
