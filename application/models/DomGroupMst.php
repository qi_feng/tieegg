<?php

class DomGroupMst extends Eloquent {

    // table名
    public static $table = "dom_group_mst";

    /**
     * 共同定义组&共同定义 (一对多)
     */
    public function domMst
    {
        return $this->has_many('DomMst');
    }

}

?>
