<?php

class Order extends Eloquent {

    // table名
    public static $table = "orders";
    // 更新时间戳
    public static $timestamps = true;

    /**
     * 订单内货品&订单 (多对一)
     */
    public function trade()
    {
        return $this->belongs_to('Trade');
    }

}

?>
