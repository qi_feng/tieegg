<?php

class DomMst extends Eloquent {

    // table名
    public static $table = "dom_mst";

    /**
     * 共同定义&共同定义组 (多对一)
     */
    public function domGroupMst()
    {
        return $this->belongs_to('DomGroupMst');
    }

}

?>
