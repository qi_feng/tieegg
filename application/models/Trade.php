<?php

class Trade extends Eloquent {

    // table名
    public static $table = "trades";
    // 更新时间戳
    public static $timestamps = true;

    /**
     * 订单&订单内货品(一对多)
     */
    public function orders()
    {
        return $this->has_many('Order');
    }

}

?>
