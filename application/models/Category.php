<?php

class Category extends Eloquent {

    // table名
    public static $table = "categorys";

    /**
     * 标签&货品 (多对多)
     */
    public function products()
    {
        return $this->has_many_and_belongs_to('Product', 'product_category');
    }

}

?>
