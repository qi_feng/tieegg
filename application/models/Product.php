<?php

class Product extends Eloquent {

    // table名
    public static $table = "products";
    // 更新时间戳
    public static $timestamps = true;

    /**
     * 货品&货品图片 (一对多)
     */
    public function productImgs()
    {
        return $this->has_many('ProductImg');
    }

    /**
     * 货品&货品说明 (一对多)
     */
    public function productInfos()
    {
        return $this->has_many('ProductInfo');
    }

    /**
     * 货品&标签 (多对多)
     */
    public function categorys()
    {
        return $this->has_many_and_belongs_to('Category', 'product_category');
    }

    /**
     * 货品&品牌 (多对一)
     */
    public function brand()
    {
        return $this->belongs_to('Brand');
    }

}

?>
