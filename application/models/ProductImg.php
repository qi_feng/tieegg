<?php

class ProductImg extends Eloquent {

    // table名
    public static $table = "product_imgs";

    /**
     * 货品图片&货品 (多对一)
     */
    public function product()
    {
        return $this->belongs_to('Product');
    }

}

?>
