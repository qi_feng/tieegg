<?php

class Category_Controller extends Base_Controller {

    public $restful = true;

    /**
     * 画面初始化_分类列表
     */
    public function get_list($flg)
    {

        $view = View::make('category.list');

        if ($flg == 'usage') { // 按类别显示分类画面
            $categorys = Category::where('category_level', '=', 2)->or_where('category_level', '=', 3)->order_by('sort')->paginate(8);
            $view->title = "类别SHOPPING";
            Session::put('cateName', '类别SHOPPING');
            Session::put('cateLink', 'usage');
        } else if ($flg == 'age') { // 按年龄显示分类画面
            $categorys = Category::where('category_level', '=', 1)->or_where('category_level', '=', 3)->order_by('sort')->paginate(8);
            $view->title = "年龄SHOPPING";
            Session::put('cateName', '年龄SHOPPING');
            Session::put('cateLink', 'age');
        } else { // 判断参数错误时
            return Response::error('404');
        }

        // 分类数据传入
        $view->cates = $categorys;
        // 显示分类画面
        return $view;
    }

    /**
     * 画面提交_分类列表
     */
    public function post_list()
    {

    }

}

?>
