<?php

class Product_Controller extends Base_Controller {

    public $restful = true;

    /**
     * 画面初始化_货品列表
     */
    public function get_list($category_id)
    {
        $view = View::make('product.list');

        // 类别详细内容检出
        $category = Category::find($category_id);

        // 全部货品选择
        if ($category_id == 13) {
            $products = Product::where('id','<>','')->paginate(12);
        } else {
            // 该类别下所属货品检出
            $products = $category->products()->paginate(12);
        }

        $view->products = $products;
        // 画面title
        $view->title = $category->category_name;
        Session::put('subCateName', $category->category_name);
        Session::put('subCateLink', $category_id);

        return $view;
    }

    /**
     * 画面提交_货品列表
     */
    public function post_list()
    {

    }

    /**
     * 画面初始化_货品详细
     */
    public function get_detail($product_id)
    {
        $view = View::make('product.detail');

        // 货品详细内容检出
        $product = Product::find($product_id);
        $view->product = $product;

        // 货品对应图片详细检出
        $productImgs = $product->productImgs;
        $view->productImgs = $productImgs;

        // 货品说明详细检出
        $productInfos = $product->productInfos;
        $view->productInfos = $productInfos;

        // 货品对应品牌检出
        $brand = $product->brand;
        $view->brand = $brand->brand_name;

        // 画面title
        $view->title = $product->product_name;

        // 尺寸
        $view->size = array('1' => 'S', '2' => 'M', '3' => 'L');

        return $view;
    }

    /**
     * 画面提交_货品详细(加入购物车)
     */
    public function post_detail()
    {
        // 根据action变量判断动作
        //
        $action = Input::get('action');
        // 货品ID
        $product_id = Input::get('item_id');
        // 根据货品ID取得货品详细内容
        //
        $product = Product::find($product_id);

        // 加入购物车货品的数量
        //
        $qty     = Input::get('qty');
        // 其他参数
        $options = array();

        // 购物车infomation做成
        //
        $item = array(
            'id'      => $product_id,
            'qty'     => $qty,
            'price'   => $product->coupon_price,
            'name'    => $product->product_name,
            'image'   => $product->product_img_url,
            'options' => $options
        );

        // 加入心愿单
        //
        if ($action === 'add_to_wishlist')
        {
            try
            {
                // 把指定货品假如心愿单
                //
                Cartify::wishlist()->insert($item);
            }

            // Check if we have invalid data passed.
            //
            catch (Cartify\CartInvalidDataException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'Invalid data passed.');
            }

            // Check if we a required index is missing.
            //
            catch (Cartify\CartRequiredIndexException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', $e->getMessage());
            }

            // Check if the quantity is invalid.
            //
            catch (Cartify\CartInvalidItemQuantityException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'Invalid item quantity.');
            }

            // Check if the item row id is invalid.
            //
            catch (Cartify\CartInvalidItemRowIdException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'Invalid item row id.');
            }

            // Check if the item name is invalid.
            //
            catch (Cartify\CartInvalidItemNameException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'Invalid item name.');
            }

            // Check if the item price is invalid.
            //
            catch (Cartify\CartInvalidItemPriceException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'Invalid item price.');
            }

            // Maybe we want to catch all the errors? Sure.
            //
            catch (Cartify\CartException $e)
            {
                // Redirect back to the home page.
                //
                return Redirect::to('cart')->with('error', 'An unexpected error occurred!');
            }

            // Redirect to the wishlist page.
            //
            return Redirect::to('cartify/wishlist')->with('success', 'The item was added to your wishlist!');
        }

        // 加入购物车
        //
        elseif ($action === 'add_to_cart')
        {
            try
            {
                // 把指定货品加入购物车
                //
                if (Sentry::check()) { // 如果当前用户登录了, 假如用户的购物车

                    Cartify::cart('user_cart_' . Sentry::user()->id)->insert($item);

                    // 同时将数据登录到DB中(登录用户数据同步)
                    $dbCartObj = ShopCarts::where('user_id', '=', Sentry::user()->id)
                                            ->where('product_id', '=', $item['id'])
                                            ->first();
                    if ($dbCartObj) {
                        $newQuantity = (int)$dbCartObj->quantity + (int)$item['qty'];
                        $dbCartObj->quantity = $newQuantity;
                        $dbCartObj->save();
                    } else {
                        $shopCartObj = new ShopCarts;
                        // 用户ID
                        $shopCartObj->user_id = Sentry::user()->id;
                        // 货品ID
                        $shopCartObj->product_id = $item['id'];
                        // 货品购买数量
                        $shopCartObj->quantity = $item['qty'];
                        // 插入数据
                        $shopCartObj->save();
                    }

                } else { // 如果当前未登录, 假如临时购物车
                    Cartify::cart()->insert($item);
                }
            }

            // Check if we have invalid data passed.
            //
            catch (Cartify\CartInvalidDataException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '错误的数据被提交.'));
            }

            // Check if we a required index is missing.
            //
            catch (Cartify\CartRequiredIndexException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => $e->getMessage()));
            }

            // Check if the quantity is invalid.
            //
            catch (Cartify\CartInvalidItemQuantityException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '购买数量不正确.'));
            }

            // Check if the item row id is invalid.
            //
            catch (Cartify\CartInvalidItemRowIdException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '货品RowId异常.'));
            }

            // Check if the item name is invalid.
            //
            catch (Cartify\CartInvalidItemNameException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '货品名称异常.'));
            }

            // Check if the item price is invalid.
            //
            catch (Cartify\CartInvalidItemPriceException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '货品价格异常.'));
            }

            // Maybe we want to catch all the errors? Sure.
            //
            catch (Cartify\CartException $e)
            {
                // Redirect back to the home page.
                //
                return Response::json(array('error' => true, 'message' => '发生未知异常.'));
            } catch(Exception $e)
            {
                return Response::json(array('error' => true, 'message' => $e->getMessage()));
            }

            // Redirect to the cart page.
            //
            return Response::json(array('error' => false, 'message' => '成功加入购物车!'));
        }

        // Invalid action, redirect to the home page.
        //
        return Response::json(array('error' => true, 'message' => '用户动作异常.'));
    }

    /************************************************              ************************************************/
    /************************************************ TODO 管理功能 ************************************************/
    /************************************************              ************************************************/

    /**
     * 画面初始化_货品追加
     */
    public function get_add()
    {
        $view = View::make('product.add');

        // 画面初期化数据做成
        // 货品分类list
        $categorys = Category::all();
        $categoryAry = array();
        foreach ($categorys as $obj) {
            $categoryAry[$obj->id] = $obj->category_name;
        }
        $view->categoryAry = $categoryAry;

        // 折扣list
        $discounts = DomMst::where('dom_group_id', '=', '1')->order_by('dom_sort', 'desc')->get();
        $discountAry = array();
        foreach ($discounts as $obj) {
            $discountAry[$obj->dom_value] = $obj->dom_name;
        }
        $view->discountAry = $discountAry;

        return $view;
    }

    /**
     * 画面提交_货品追加
     */
    public function post_add()
    {

    }

}

?>
