<?php

class Order_Controller extends Base_Controller {

    public $restful = true;

    /**
     * 订单收货地址确认画面初始化
     */
    public function get_deliveryconfirm()
    {
        $view = View::make('order.deliveryconfirm');
        $view->title = '收货地址确认';
        $view->user = Sentry::user();

        /* 送货地址 */
        $userId = $view->user->id;
        // 送货地址取得
        $deliveryAddrs = UserAddrs::where('user_id', '=', $userId)->order_by('id', 'asc')->get();
        $view->deliveryAddrs = $deliveryAddrs;

        // 主地址序号
        $mainNum = 1;
        for ($i=0;$i < sizeof($deliveryAddrs);$i++) {
            if ($deliveryAddrs[$i]->main_addr == 1) {
                $mainNum = $i + 1;
            }
        }

        $view->mainAddrNum = $mainNum;

        // 订单生成画面迁移
        return $view;
    }

    /**
     * 订单收货地址确认画面提交
     */
    public function post_deliveryconfirm()
    {
        // validation规则
        $rules = array(
            'guestname' => 'required|between:1,20'
        );

        // validate提交数据
        $validation = Validator::make(Input::all(), $rules);
        // 输入数据一次性写入Session
        //Input::flash();

        // 执行validation
        if ($validation->valid())
        {
            // 整理数据
            $deliveryAddr = array();
            $deliveryAddr['name'] = Input::get('guestname');
            $deliveryAddr['province'] = Input::get('province');
            $deliveryAddr['city'] = Input::get('city');
            $deliveryAddr['area'] = Input::get('area');
            $deliveryAddr['address'] = Input::get('address');
            $deliveryAddr['post_code'] = Input::get('postcode');
            $deliveryAddr['contact_phone'] = Input::get('contactphone');

            Session::put('orderAddr', $deliveryAddr);

            // 保存收货地址
            if (Input::get('addrCheck') == 1) {
                // 整理数据
                $userAddr = new UserAddrs;
                $userAddr->user_id = Sentry::user()->id;
                $userAddr->main_addr = 0;
                $userAddr->name = Input::get('guestname');
                $userAddr->province = Input::get('province');
                $userAddr->city = Input::get('city');
                $userAddr->area = Input::get('area');
                $userAddr->address = Input::get('address');
                $userAddr->post_code = Input::get('postcode');
                $userAddr->contact_phone = Input::get('contactphone');

                $userAddr->save();
            }

            return Redirect::to('/paytypechoose');

        } else {

            // 提交数据不正确,返回画面
            return Redirect::to('/deliveryconfirm')->with_errors($validation)->with_input();

        }
    }

    /**
     * 订单支付方式确认画面初始化
     */
    public function get_paytype()
    {
        $view = View::make('order.paytype');
        $view->title = '支付方式确认';
        $view->user = Sentry::user();

        // 订单支付方式画面迁移
        return $view;
    }

    /**
     * 订单支付方式确认画面提交
     */
    public function post_paytype()
    {

    }

    /**
     * 订单详情确认画面初始化
     */
    public function get_orderconfirm($paytype)
    {
        $view = View::make('order.orderconfirm');
        $view->title = '订单详情确认';
        $view->user = Sentry::user();

        $cart_contents = Cartify::cart('user_cart_' . Sentry::user()->id)->contents();

        $view->cart_contents = $cart_contents;

        $cart_name = 'user_cart_' . Sentry::user()->id;
        $view->cart_name = $cart_name;

        $view->paytype = $paytype;

        // 订单支付方式画面迁移
        return $view;
    }

    /**
     * 订单详情确认画面提交
     */
    public function post_orderconfirm($paytype)
    {

        // 购物车ID
        $cart_name = 'user_cart_' . Sentry::user()->id;

        $trade_items = Cartify::cart($cart_name)->contents();

        // 购物是空的
        if (sizeof($trade_items) <= 0) {
            // 购物车跳转
            return Redirect::to('/cart')->with('error', '对不起,请不要重复提交订单或提交空的订单.');
        }

        /**************************支付宝基本配置**************************/
        //合作身份者id，以2088开头的16位纯数字
        $alipay_config['partner'] = '2088702759467669';

        //安全检验码，以数字和字母组成的32位字符
        $alipay_config['key'] = 'v9wfn775ejeitjhsilqvgr17l67reikm';

        //签名方式 不需修改
        $alipay_config['sign_type'] = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //ca证书路径地址，用于curl中ssl校验
        //请保证cacert.pem文件在当前文件夹目录中
        $alipay_config['cacert'] = path('app').'libraries/alipay/cacert.pem';

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport'] = 'http';

        /**************************请求参数**************************/
        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = "http://www.tieegg.com/alipay_notify_url";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = "http://www.tieegg.com/alipay_return_url";
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        //卖家支付宝帐户
        //$seller_email = $_POST['WIDseller_email'];
        $seller_email = 'fengq1984@gmail.com';
        //必填

        //商户订单号
        //$out_trade_no = $_POST['WIDout_trade_no'];
        $out_trade_no = date("YmdHis") . rand(10, 99);
        //商户网站订单系统中唯一订单号，必填

        //订单名称
        //$subject = $_POST['WIDsubject'];
        $subject = '铁蛋壳订单' . $out_trade_no;
        //必填

        //付款金额
        //$price = $_POST['WIDprice'];
        $price = format_number(Cartify::cart($cart_name)->total());
        //$price = 0.01;
        //必填

        //商品数量
        $quantity = 1;
        //必填，建议默认为1，不改变值，把一次交易看成是一次下订单而非购买一件商品

        //物流费用
        if (Cartify::cart($cart_name)->total_items() >= 3) {
            $logistics_fee = "0.00";
        } else {
            $logistics_fee = "10.00";
        }
        //必填，即运费

        //物流类型
        $logistics_type = "EXPRESS";
        //必填，三个值可选：EXPRESS（快递）、POST（平邮）、EMS（EMS）
        //物流支付方式
        $logistics_payment = "BUYER_PAY";
        //必填，两个值可选：SELLER_PAY（卖家承担运费）、BUYER_PAY（买家承担运费）

        //订单描述
        //$body = $_POST['WIDbody'];
        $body = '';

        //商品展示地址
        //$show_url = $_POST['WIDshow_url'];
        $show_url = 'http://www.tieegg.com';
        //需以http://开头的完整路径，如：http://www.xxx.com/myorder.html

        $orderAddr = Session::get('orderAddr');

        //收货人姓名
        //$receive_name = $_POST['WIDreceive_name'];
        $receive_name = $orderAddr['name'];
        //如：张三

        //收货人地址
        //$receive_address = $_POST['WIDreceive_address'];
        $receive_address = '';
        $receive_address = $receive_address . $orderAddr['province'];
        $receive_address = $receive_address . $orderAddr['city'];
        $receive_address = $receive_address . $orderAddr['area'];
        $receive_address = $receive_address . $orderAddr['address'];
        //如：XX省XXX市XXX区XXX路XXX小区XXX栋XXX单元XXX号

        //收货人邮编
        //$receive_zip = $_POST['WIDreceive_zip'];
        $receive_zip = $orderAddr['post_code'];
        //如：123456

        //收货人电话号码
        //$receive_phone = $_POST['WIDreceive_phone'];
        $receive_phone = '';
        //如：0571-88158090

        //收货人手机号码
        //$receive_mobile = $_POST['WIDreceive_mobile'];
        $receive_mobile = $orderAddr['contact_phone'];
        //如：13312341234

        if ($paytype == 'payonline') {

            $alipaySubmit = new AlipaySubmit($alipay_config);

            $parameter = array(
                "service" => "create_partner_trade_by_buyer",
                "partner" => trim($alipay_config['partner']),
                "payment_type"  => $payment_type,
                "notify_url"    => $notify_url,
                "return_url"    => $return_url,
                "seller_email"  => $seller_email,
                "out_trade_no"  => $out_trade_no,
                "subject"   => $subject,
                "price" => $price,
                "quantity"  => $quantity,
                "logistics_fee" => $logistics_fee,
                "logistics_type"    => $logistics_type,
                "logistics_payment" => $logistics_payment,
                "body"  => $body,
                "show_url"  => $show_url,
                "receive_name"  => $receive_name,
                "receive_address"   => $receive_address,
                "receive_zip"   => $receive_zip,
                "receive_phone" => $receive_phone,
                "receive_mobile"    => $receive_mobile,
                "_input_charset"    => trim(strtolower($alipay_config['input_charset']))
            );

            // 支付宝跳转画面初始化
            $html_text = $alipaySubmit->buildRequestForm($parameter, 'post', '跳转支付宝ing......');

            // 订单生成(数据库更新)
            $trade = new Trade;
            // 用户ID
            $trade->user_id = Sentry::user()->id;
            // 铁蛋壳订单号
            $trade->my_trade_no = $out_trade_no;
            // 阿里订单号
            $trade->ali_trade_no = 0;
            // 购买货品种类数
            $trade->items_sum = Cartify::cart($cart_name)->total_items();
            // 订单状态(1:等待用户付款)
            $trade->trade_status = 1;
            // 收货人
            $trade->delivery_name = $receive_name;
            // 收货地址
            $trade->delivery_address = $receive_address . ' ' . $receive_zip;
            // 收货联系电话
            $trade->delivery_phone = $receive_mobile;
            // 支付方式(1:在线支付)
            $trade->pay_type = 1;
            // 货品总金额
            $trade->total_price = $price;
            // 运费
            $trade->delivery_price = $logistics_fee;
            // 保存订单数据
            $trade->save();

            // 订单中货品数据保存
            foreach($trade_items as $trade_item)
            {
                $order = new Order;
                $order->trade_id = $trade->id;
                $order->product_id = $trade_item['id'];
                $order->quantity = $trade_item['qty'];
                $order->sell_price = $trade_item['price'];
                $order->sum_price = $trade_item['subtotal'];
                $order->save();
            }

            // 订单相关session数据清除
            // 收货地址clear
            if (Session::has('orderAddr')) {
                Session::forget('orderAddr');
            }
            // 购物车clear
            Cartify::cart($cart_name)->destroy();
            ShopCarts::where('user_id', '=', Sentry::user()->id)->delete();

            // mail通知用户订单信息

            // 跳转到支付宝画面
            return $html_text;

        } else {

            // 订单生成(数据库更新)
            $trade = new Trade;
            // 用户ID
            $trade->user_id = Sentry::user()->id;
            // 铁蛋壳订单号
            $trade->my_trade_no = $out_trade_no;
            // 阿里订单号
            $trade->ali_trade_no = 0;
            // 购买货品种类数
            $trade->items_sum = Cartify::cart($cart_name)->total_items();
            // 订单状态(2:备货中)
            $trade->trade_status = 2;
            // 收货人
            $trade->delivery_name = $receive_name;
            // 收货地址
            $trade->delivery_address = $receive_address . ' ' . $receive_zip;
            // 收货联系电话
            $trade->delivery_phone = $receive_mobile;
            // 支付方式(2:货到付款)
            $trade->pay_type = 2;
            // 货品总金额
            $trade->total_price = $price;
            // 运费
            $trade->delivery_price = $logistics_fee;
            // 保存订单数据
            $trade->save();

            // 订单中货品数据保存
            foreach($trade_items as $trade_item)
            {
                $order = new Order;
                $order->trade_id = $trade->id;
                $order->product_id = $trade_item['id'];
                $order->quantity = $trade_item['qty'];
                $order->sell_price = $trade_item['price'];
                $order->sum_price = $trade_item['subtotal'];
                $order->save();
            }

            // 订单相关session数据清除
            // 收货地址clear
            if (Session::has('orderAddr')) {
                Session::forget('orderAddr');
            }
            // 购物车clear
            Cartify::cart($cart_name)->destroy();
            ShopCarts::where('user_id', '=', Sentry::user()->id)->delete();

            // mail通知用户订单信息

            // 跳转到我的订单画面
            Session::flash('success_flg', true);
            Session::flash('success_msg', '您的订单(' . $out_trade_no . ')已经提交到铁蛋壳, 祝您购物愉快.');
            return Redirect::to('/userinfo')->with('tabHref', '#mytrade');

        }

    }

    /**
     * 支付宝同步通知(GET方式调用)
     * 买家支付成功后会调用该方法
     * 该方式仅仅在买家付款完成以后进行自动跳转，因此只会进行一次。
     * @return 我的订单画面
     */
    public function get_alipayreturn()
    {

        /**************************支付宝基本配置**************************/
        //合作身份者id，以2088开头的16位纯数字
        $alipay_config['partner'] = '2088702759467669';

        //安全检验码，以数字和字母组成的32位字符
        $alipay_config['key'] = 'v9wfn775ejeitjhsilqvgr17l67reikm';

        //签名方式 不需修改
        $alipay_config['sign_type'] = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //ca证书路径地址，用于curl中ssl校验
        //请保证cacert.pem文件在当前文件夹目录中
        $alipay_config['cacert'] = path('app').'libraries/alipay/cacert.pem';

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport'] = 'http';

        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();

        //验证成功
        if($verify_result) {

            // 更新订单状态(等待买家付款 -> 买家已付款)
            //商户订单号
            $out_trade_no = Input::get('out_trade_no');

            //支付宝交易号
            $trade_no = Input::get('trade_no');

            //交易状态
            $trade_status = Input::get('trade_status');

            //判断该笔订单是否在商户网站中已经做过处理
            // WAIT_SELLER_SEND_GOODS -> 买家已付款，等待卖家发货
            if($trade_status == 'WAIT_SELLER_SEND_GOODS') {

                // 取得当前订单信息
                $trade = Trade::where('my_trade_no', '=', $out_trade_no)->first();

                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                // 判断订单状态(如果订单状态为等待用户付款)
                if ($trade->trade_status == 1) {
                    // 订单状态变更为 2:备货中
                    $trade->trade_status = 2;
                    // 阿里订单号
                    $trade->ali_trade_no = $trade_no;
                    // 更新订单
                    $trade->save();
                } else { //如果有做过处理，不执行商户的业务程序

                }

                // 支付成功信息做成
                Session::flash('warning_flg', true);
                Session::flash('warning_msg', '您的订单(' . $out_trade_no . ')在线支付成功!');

            }

            // 跳转到我的订单画面
            // 订单生成成功信息做成
            Session::flash('success_flg', true);
            Session::flash('success_msg', '您的订单(' . $out_trade_no . ')已经提交到铁蛋壳, 祝您购物愉快.');
            return Redirect::to('/userinfo')->with('tabHref', '#mytrade');

        } else {

            // 验证错误 error画面迁移
            return Redirect::to('404');

        }
    }

    /**
     * 支付宝异步通知(POST方式调用)
     * 订单的支付宝状态每次改变时都调用该方法
     * @return 打印输出“success”,否则会不断调用该方法
     */
    public function post_alipaynotify()
    {

        /**************************支付宝基本配置**************************/
        //合作身份者id，以2088开头的16位纯数字
        $alipay_config['partner'] = '2088702759467669';

        //安全检验码，以数字和字母组成的32位字符
        $alipay_config['key'] = 'v9wfn775ejeitjhsilqvgr17l67reikm';

        //签名方式 不需修改
        $alipay_config['sign_type'] = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //ca证书路径地址，用于curl中ssl校验
        //请保证cacert.pem文件在当前文件夹目录中
        $alipay_config['cacert'] = path('app').'libraries/alipay/cacert.pem';

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport'] = 'http';

        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        //验证成功
        if($verify_result) {

            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表

            //商户订单号
            $out_trade_no = Input::get('out_trade_no');

            //支付宝交易号
            $trade_no = Input::get('trade_no');

            //交易状态
            $trade_status = Input::get('trade_status');

            // 取得当前订单信息
            $trade = Trade::where('my_trade_no', '=', $out_trade_no)->first();

            //该判断表示买家已在支付宝交易管理中产生了交易记录，但没有付款
            if($trade_status == 'WAIT_BUYER_PAY') {

                //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                // 阿里订单号
                $trade->ali_trade_no = $trade_no;
                // 更新订单
                $trade->save();

                echo "success";     //请不要修改或删除

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
            }
            else if($trade_status == 'WAIT_SELLER_SEND_GOODS') {
            //该判断表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货

                //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                // 判断订单状态(如果订单状态为等待用户付款)
                if ($trade->trade_status == 1) {
                    // 订单状态变更为 2:备货中
                    $trade->trade_status = 2;
                    // 阿里订单号
                    $trade->ali_trade_no = $trade_no;
                    // 更新订单
                    $trade->save();
                }

                echo "success";     //请不要修改或删除

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
            }
            else if($trade_status == 'WAIT_BUYER_CONFIRM_GOODS') {
            //该判断表示卖家已经发了货，但买家还没有做确认收货的操作

                //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                // 订单状态变更为 3:已发货
                $trade->trade_status = 3;
                // 更新订单
                $trade->save();

                echo "success";     //请不要修改或删除

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
            }
            else if($trade_status == 'TRADE_FINISHED') {
            //该判断表示买家已经确认收货，这笔交易完成

                //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                // 订单状态变更为 4:交易完成
                $trade->trade_status = 4;
                // 更新订单
                $trade->save();

                echo "success";     //请不要修改或删除

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
            }
            else {

                // 订单状态变更为 5:交易异常
                $trade->trade_status = 5;
                // 更新订单
                $trade->save();

                //其他状态判断
                echo "success";

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult ("这里写入想要调试的代码变量值，或其他运行的结果记录");
            }

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

        }
        else {
            //验证失败
            echo "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }

    }

}

?>
