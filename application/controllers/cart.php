<?php

class Cart_Controller extends Base_Controller {

	public $restful = true;

	public function get_cart()
	{
		// 取得购物车内容
		//
		$cart_contents;
		$cart_name = null;
		if (Sentry::check()) { // 用户已经登录,从用户购物车读取信息.
			$cart_contents = Cartify::cart('user_cart_' . Sentry::user()->id)->contents();

			if (sizeof($cart_contents) <= 0) {
				// 当前登录用户的购物车信息
		        $shopCartInfos = ShopCarts::where('user_id', '=', Sentry::user()->id)->get();

		        // 数据库中存在用户购物车数据的时候
		        if (sizeOf($shopCartInfos) > 0) {

		            // 初始化用户购物车
		            foreach ($shopCartInfos as $obj) {
		                $product_id = $obj->product_id;
		                $qty = $obj->quantity;
		                $product = Product::find($product_id);
		                $item = array(
		                    'id'      => $product_id,
		                    'qty'     => $qty,
		                    'price'   => $product->coupon_price,
		                    'name'    => $product->product_name,
		                    'image'   => $product->product_img_url,
		                    'options' => array()
		                );
		                Cartify::cart('user_cart_' . Sentry::user()->id)->insert($item);
		            }

		            $cart_contents = Cartify::cart('user_cart_' . Sentry::user()->id)->contents();
		        }
			}
			$cart_name = 'user_cart_' . Sentry::user()->id;
		} else {
			$cart_contents = Cartify::cart()->contents();
		}

		// 主页画面表示
		return View::make('cart.cart')
						->with('cart_contents', $cart_contents)
						->with('cart_name', $cart_name)
						->with('title', '蛋购袋');
	}

	/**
	 * 更新,清空购物车货品
	 *
	 * @access   public
	 * @return   void
	 */
	public function post_cart()
	{
		// 更新购物车
		//
		if (Input::get('update'))
		{
			try
			{
				// Get the items to be updated.
				//
				$items = array();
				foreach(Input::get('items') as $rowid => $qty)
				{
					$items[] = array(
						'rowid' => $rowid,
						'qty'   => $qty
					);
				}

				// 更新购物车.
				//
				if (Sentry::check()) { // 用户已经登录,更新用户购物车信息.
					Cartify::cart('user_cart_' . Sentry::user()->id)->update($items);
					ShopCarts::where('user_id', '=', Sentry::user()->id)->delete();
					$db_items = Cartify::cart('user_cart_' . Sentry::user()->id)->contents();
					foreach($db_items as $db_item)
					{
						$shopCartObj = new ShopCarts;
						$shopCartObj->user_id = Sentry::user()->id;
						$shopCartObj->product_id = $db_item['id'];
						$shopCartObj->quantity = $db_item['qty'];
						$shopCartObj->save();
					}
				} else { // 用户未登录,更新临时购物车信息.
					Cartify::cart()->update($items);
				}
			}

			// Is the Item Row ID valid?
			//
			catch (Cartify\CartInvalidItemRowIdException $e)
			{
				// Redirect back to the shopping cart page.
				//
				return Redirect::to('/cart')->with('error', '错误的货品ID!');
			}

			// Does this item exists on the shopping cart?
			//
			catch (Cartify\CartItemNotFoundException $e)
			{
				// Redirect back to the shopping cart page.
				//
				return Redirect::to('/cart')->with('error', '蛋购袋里没找到该货品!');
			}

			// Is the item quantity valid?
			//
			catch (Cartify\CartInvalidItemQuantityException $e)
			{
				// Redirect back to the shopping cart page.
				//
				return Redirect::to('/cart')->with('error', '购买数量不对!');
			}

			// Redirect back to the shopping cart page.
			//
			return Redirect::to('/cart')->with('success', '蛋购袋已经更新了.');
		}

		// 清空购物车
		//
		elseif (Input::get('empty'))
		{
			// Let's clear the shopping cart!
			//
			if (Sentry::check()) { // 用户已经登录,清空用户购物车信息.
				Cartify::cart('user_cart_' . Sentry::user()->id)->destroy();
				ShopCarts::where('user_id', '=', Sentry::user()->id)->delete();
			} else { // 用户未登录,清空临时购物车信息.
				Cartify::cart()->destroy();
			}

			// Redirect back to the shopping cart page.
			//
			return Redirect::to('/cart')->with('success', '蛋购袋被清空了!');
		}
	}

	/**
	 * 移除购物车某一货品
	 *
	 * @access   public
	 * @param    string row_id
	 * @return   void
	 */
	public function get_remove($row_id = null)
	{
		try
		{
			// 移除购物车货品 BY ID.
			//
			if (Sentry::check()) { // 用户已经登录,移除用户购物车货品.
				$deleteCartObj = Cartify::cart('user_cart_' . Sentry::user()->id)->item($row_id);
				// 用户DB购物车数据同步
				$shopCartObj = ShopCarts::where('user_id', '=', Sentry::user()->id)
										->where('product_id', '=', $deleteCartObj['id']);
				$shopCartObj->delete();

				Cartify::cart('user_cart_' . Sentry::user()->id)->remove($row_id);
			} else { // 用户未登录,移除临时购物车货品.
				Cartify::cart()->remove($row_id);
			}
		}

		// Is the Item Row ID valid?
		//
		catch (Cartify\CartInvalidItemRowIdException $e)
		{
			// Redirect back to the shopping cart page.
			//
			return Redirect::to('/cart')->with('error', '错误的货品ID!');
		}

		// Does this item exists on the shopping cart?
		//
		catch (Cartify\CartItemNotFoundException $e)
		{
			// Redirect back to the shopping cart page.
			//
			return Redirect::to('/cart')->with('error', '蛋购袋里没找到该货品!');
		}

		// Other error.
		//
		catch (Cartify\CartException $e)
		{
			// Redirect back to the home page.
			//
			return Redirect::to('/cart')->with('error', '发生未知错误!');
		}

		// Redirect back to the shopping cart page.
		//
		return Redirect::to('/cart')->with('success', '货品已经从蛋购袋中移除了.');
	}

}
