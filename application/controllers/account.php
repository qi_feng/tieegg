<?php

class Account_Controller extends Base_Controller {

    public $restful = true;

    /**
     * 用户登录画面初始化
     */
    public function get_signin()
    {
        // sina用户初始化
        $sinaObj = new SaeTOAuthV2( '1864491839' , 'e433595a2f74b48b15713da46f490baa' );
        // sina认证url作成
        $code_url = $sinaObj->getAuthorizeURL( 'http://www.tieegg.com/sinaSignin' );

        // check是否登录, true时
        if (Sentry::check()) {
            // 主画面迁移
            return Redirect::to('');
        } else { // false时
            // 登录画面迁移
            return View::make('account.signin')
                            ->with('email', Input::old('email'))
                            ->with('title', '登录')
                            ->with('code_url', $code_url);
        }
    }

    /**
     * 用户登录画面提交
     */
    public function post_signin()
    {

        // 验证数据取得
        $email = Input::get('email');
        $password = Input::get('password');
        $remember = Input::get('remember');

        try {

            if (Sentry::login($email, $password, !empty($remember) ? true : false)) {
                // 购物车数据初始化
                static::exchangeCartInfo();
                // 成功:迁移主画面
                return Redirect::to('/');
            } else {
                // 输入数据一次性写入Session
                Input::flash();
                // 失败:登录画面保持,error表示
                Session::flash('error_flg', true);
                Session::flash('error_msg', '系统异常.');
                return Redirect::to('signin');
            }

        } catch (Sentry\SentryException $e) {
                // 输入数据一次性写入Session
                Input::flash();
                // 失败:登录画面保持,error表示
                Session::flash('error_flg', true);
                Session::flash('error_msg', '用户名或密码不正确, 请再次输入.');
                return Redirect::to('signin');
        }

    }

    /**
     * 用户注册画面初始化
     */
    public function get_signup()
    {
        $view = View::make('account.signup');
        $view->title = '注册蛋友';
        return $view;
    }

    /**
     * 用户注册画面提交
     */
    public function post_signup()
    {

        // validation规则
        $rules = array(
            'nickname' => 'match:/^[0-9a-zA-Z\x{4e00}-\x{9fa5}_-]+$/u|required',
            'email' => 'email|required|max:40',
            'password' => 'same:repassword|required|between:6,18',
            'repassword' => 'required|between:6,18',
            'captcha' => 'coolcaptcha|required'
        );

        // validate提交数据
        $validation = Validator::make(Input::all(), $rules);
        // 输入数据一次性写入Session(密码不被保留显示)
        Input::flash('except', array('password', 'repassword'));

        // 执行validation
        if ($validation->valid())
        {
            // 如果注册信息已经存在了
            if (sizeof(User::where('username', '=', Input::get('nickname'))->get()) > 0) {
                // 昵称已经被注册了
                $validation->errors->add('nickname', '输入的昵称已经被注册了!');
            }

            if (Sentry::user_exists(Input::get('email'))) {
                // $errorMsg = '输入的E-Mail地址已经被注册了!';
                $validation->errors->add('email', '输入的E-Mail地址已经被注册了!');
            }

            if ($validation->errors->all()) {
                // 提交数据不正确,返回注册画面
                return Redirect::to('signup')->with_errors($validation)->with_input();
            }

            try
            {
                // 整理注册用数据
                $vars = array(
                    'username' => Input::get('nickname'),
                    'email'    => Input::get('email'),
                    'password' => Input::get('password'),
                    'metadata' => array(
                        'first_name' => 'first',
                        'last_name'  => 'last',
                        // 自定义字段添加在这里
                        'sex' => 0,
                        'baby_status' => 0,
                        'baby_age' => 0,
                        'baby_sex' => 0,
                        'social_type' => 0,
                    )
                );

                // 注册用户
                if ($user_id = Sentry::user()->create($vars))
                {
                    // 注册成功,主页迁移
                    if (Sentry::login($vars['email'], $vars['password'], true)) {

                        Message::send(function($message) {
                            $message->to(array(Input::get('email') => Input::get('nickname')));
                            $message->from('tieegg.service@gmail.com', '铁蛋壳 tieegg.com');
                            $message->subject('铁蛋壳(www.tieegg.com)注册成功确认！');

                            $bodystr = '';
                            $bodystr = $bodystr . '亲爱的蛋友: <b>'. Input::get('nickname') .'</b><br>';
                            $bodystr = $bodystr . '&nbsp;&nbsp;感谢你注册铁蛋壳!<br>';
                            $bodystr = $bodystr . '&nbsp;&nbsp;你的登录邮箱是: <a href="mailto:'. Input::get('email') .'">'. Input::get('email') .'</a><br>';
                            $bodystr = $bodystr . '&nbsp;&nbsp;登录密码: '. Input::get('password') .'<br><br><hr>';
                            $bodystr = $bodystr . '点击下面链接, 体验本地母婴在线购物的畅快感受吧!<br>';
                            $bodystr = $bodystr . '<a href="http://www.tieegg.com">http://www.tieegg.com</a><br>';
                            $bodystr = $bodystr . '如有任何问题, 可以直接回复本邮件咨询管理员.';
                            $message->body($bodystr);
                            $message->html(true);
                        });

                        if(Message::was_sent()) {
                            // 注册成功,用户信息画面迁移
                            // 成功:success表示
                            Session::flash('success_flg', true);
                            Session::flash('success_msg', '亲爱的蛋友, 感谢您注册铁蛋壳, 体验本地母婴在线购物的畅快感受吧!<br>您的邮箱['. Input::get('email') .']会收到一封成功邮件.');
                        }

                        // 购物车数据初始化
                        static::exchangeCartInfo();

                        return Redirect::to('/userinfo');
                    } else {
                        return Response::error('500');
                    }
                }
                else
                {
                    // 出错了,500画面迁移(正常不会到这里)
                    return Response::error('500');
                }
            }
            catch (Sentry\SentryException $e)
            {
                $errors = $e->getMessage();
            }

        } else {
            // 提交数据不正确,返回注册画面
            return Redirect::to('signup')->with_errors($validation)->with_input();
        }

    }

    /**
     * 用户详细画面初始化
     */
    public function get_userinfo()
    {

        $view = View::make('account.userinfo');
        $view->title = '蛋友详细';
        $view->user = Sentry::user();

        /* 蛋友资料TAB */
        // 性别
        $sexs = DomMst::where('dom_group_id', '=', '3')->order_by('dom_sort', 'asc')->get();
        // 未设定性别时,加入未设定选项
        if ($view->user->get('metadata.sex') == 0) {
            $sexAry = array(0 => '未设定');
        }
        foreach ($sexs as $obj) {
            $sexAry[$obj->dom_value] = $obj->dom_name;
        }
        $view->sexAry = $sexAry;

        // 状态
        $status = DomMst::where('dom_group_id', '=', '5')->order_by('dom_sort', 'asc')->get();
        // 未设定状态时,加入未设定选项
        if ($view->user->get('metadata.baby_status') == 0) {
            $babyStatusAry = array(0 => '未设定');
        }
        foreach ($status as $obj) {
            $babyStatusAry[$obj->dom_value] = $obj->dom_name;
        }
        $view->babyStatusAry = $babyStatusAry;

        // 蛋宝性别
        $babySexs = DomMst::where('dom_group_id', '=', '4')->order_by('dom_sort', 'asc')->get();
        // 未设定蛋宝性别时,加入未设定选项
        if ($view->user->get('metadata.baby_sex') == 0) {
            $babySexAry = array(0 => '未设定');
        }
        foreach ($babySexs as $obj) {
            $babySexAry[$obj->dom_value] = $obj->dom_name;
        }
        $view->babySexAry = $babySexAry;

        // 蛋宝年龄
        $babyAge = DomMst::where('dom_group_id', '=', '6')->order_by('dom_sort', 'asc')->get();
        // 未设定蛋宝年龄时,加入未设定选项
        if ($view->user->get('metadata.baby_age') == 0) {
            $babyAgeAry = array(0 => '未设定');
        }
        foreach ($babyAge as $obj) {
            $babyAgeAry[$obj->dom_value] = $obj->dom_name;
        }
        $view->babyAgeAry = $babyAgeAry;

        /* 送货地址Tab */
        $userId = $view->user->id;
        // 送货地址取得
        $deliveryAddrs = UserAddrs::where('user_id', '=', $userId)->order_by('id', 'asc')->get();
        $view->deliveryAddrs = $deliveryAddrs;

        /* 我的订单 */
        // 订单信息检索
        $trades = Trade::where('user_id', '=', $userId)->order_by('created_at', 'desc')->get();
        $view->trades = $trades;

        return $view;
    }

    /**
     * 用户详细修改tab提交
     */
    public function post_userinfo()
    {

        // validation规则
        $rules = array(
            'nickname' => 'match:/^[0-9a-zA-Z\x{4e00}-\x{9fa5}_-]+$/u|required'
        );

        // validate提交数据
        $validation = Validator::make(Input::all(), $rules);
        // 输入数据一次性写入Session(密码不被保留显示)
        Input::flash();

        // 当前登录用户取得
        $user = Sentry::user();

        // 执行validation
        if ($validation->valid())
        {
            // 如果注册信息已经存在了
            if (sizeof(User::where('username', '=', Input::get('nickname'))->where('id', '<>', $user->id)->get()) > 0) {
                // 昵称已经被注册了
                $validation->errors->add('nickname', '输入的昵称已经被注册了!');
            }

            if ($validation->errors->all()) {
                // 提交数据不正确,返回用户详细画面
                return Redirect::to('/userinfo')->with_errors($validation)->with_input();
            }

            try
            {
                // 整理注册用数据
                $user_data = array(
                    'username' => Input::get('nickname'),
                    'metadata' => array(
                        'sex' => Input::get('sex'),
                        'baby_status'  => Input::get('babyStatus'),
                        'baby_sex'  => Input::get('babySex'),
                        'baby_age'  => Input::get('babyAge')
                    )
                );

                // 修改用户信息
                if ($user->update($user_data))
                {
                    // 修改成功,用户信息画面迁移
                    // 成功:success表示
                    Session::flash('success_flg', true);
                    Session::flash('success_msg', '用户资料修改成功!');
                    return Redirect::to('/userinfo')->with('tabHref', '#userInfo');
                }
                else
                {
                    // 出错了,500画面迁移(正常不会到这里)
                    return Response::error('500');
                }
            }
            catch (Sentry\SentryException $e)
            {
                $errors = $e->getMessage();
            }

        } else {
            // 提交数据不正确,返回注册画面
            return Redirect::to('/userinfo')->with_errors($validation)->with_input();
        }
    }

    /**
     * 密码修改tab提交
     */
    public function post_passchange()
    {

        // validation规则
        $rules = array(
            'newpassword' => 'same:repassword|required|between:6,18',
            'repassword' => 'required|between:6,18'
        );

        // validate提交数据
        $validation = Validator::make(Input::all(), $rules);

        // 当前登录用户取得
        $user = Sentry::user();

        // 执行validation
        if ($validation->valid())
        {
            try
            {
                // 修改用户密码
                if ($user->change_password(Input::get('newpassword'), Input::get('prepassword')))
                {
                    // 修改成功,用户信息画面迁移
                    // 成功:success表示
                    Session::flash('success_flg', true);
                    Session::flash('success_msg', '密码修改成功!');
                    return Redirect::to('/userinfo')->with('tabHref', '#passChange');
                }
                else
                {
                    // 出错了,500画面迁移(正常不会到这里)
                    return Response::error('500');
                }
            }
            catch (Sentry\SentryException $e)
            {
                // 输入的原密码不正确
                $validation->errors->add('prepassword', '输入的原始密码不正确, 请重新输入!');
                return Redirect::to('/userinfo')->with_errors($validation)->with('tabHref', '#passChange');
            }

        } else {
            // 提交数据不正确,返回注册画面
            return Redirect::to('/userinfo')->with_errors($validation)->with('tabHref', '#passChange');
        }
    }

    /**
     * 送货地址追加tab提交
     */
    public function post_deliveryadd()
    {
        // validation规则
        $rules = array(
            'guestname' => 'required|between:1,20'
        );

        // validate提交数据
        $validation = Validator::make(Input::all(), $rules);
        // 输入数据一次性写入Session
        Input::flash();

        // 当前登录用户取得
        $user = Sentry::user();

        // 执行validation
        if ($validation->valid())
        {
            try
            {
                // 整理数据
                $userAddr = new UserAddrs;
                $userAddr->user_id = $user->id;
                $userAddr->main_addr = 0;
                $userAddr->name = Input::get('guestname');
                $userAddr->province = Input::get('province');
                $userAddr->city = Input::get('city');
                $userAddr->area = Input::get('area');
                $userAddr->address = Input::get('address');
                $userAddr->post_code = Input::get('postcode');
                $userAddr->contact_phone = Input::get('contactphone');

                $userAddr->save();

                // 地址追加成功
                // 成功:success表示
                Session::flash('success_flg', true);
                Session::flash('success_msg', '地址追加成功!');
                return Redirect::to('/userinfo')->with('tabHref', '#deliveryAddr');
            }
            catch (Sentry\SentryException $e)
            {
                $errors = $e->getMessage();
            }

        } else {
            // 提交数据不正确,返回画面
            return Redirect::to('/userinfo')->with_errors($validation)->with_input()->with('tabHref', '#deliveryAddr');
        }
    }

    /**
     * 设置默认收货地址
     */
    public function post_setdefaultaddr()
    {
        $addrId = Input::get('addrId');

        // 原默认地址->普通地址
        $defaultAddr = UserAddrs::where('user_id', '=', Sentry::user()->id)
                                ->where('main_addr', '=', 1)->first();
        if ($defaultAddr) {
            $defaultAddr->main_addr = 0;
            $defaultAddr->save();
        }

        // 选择的地址->默认地址
        $userAddr = UserAddrs::find($addrId);
        $userAddr->main_addr = 1;
        $userAddr->save();

        Session::flash('tabHref', '#deliveryAddr');

        return Response::json(array('error' => false));
    }

    /**
     * 删除选择的收货地址
     */
    public function post_removeaddr()
    {
        $addrId = Input::get('addrId');

        // 删除选择的地址
        $userAddr = UserAddrs::find($addrId);
        $userAddr->delete();

        Session::flash('tabHref', '#deliveryAddr');

        return Response::json(array('error' => false));
    }

    /**
     * 送货地址修改tab提交
     */
    public function post_deliverychange()
    {

    }

    /**
     * 根据订单号显示订单详细内容(AJAX异步调用)
     */
    public function post_mytrade()
    {
        $userId = Sentry::user()->id;
        $trade_id = Input::get('trade_no');
        $trade = Trade::where('my_trade_no', '=', $trade_id)->first();
        $trade_orders = $trade->orders()->get();

        $tradeArray = $trade->to_array();
        $ordersArray = array();
        for ($i = 0; $i < sizeOf($trade_orders); $i++) {
            $orderAry = $trade_orders[$i]->to_array();
            $product = Product::find($orderAry['product_id']);
            $orderAry['product_name'] = $product->product_name;
            $ordersArray[$i] = $orderAry;
        }

        return Response::json(array('error' => false, 'tradeInfo' => $tradeArray, 'ordersInfo' => $ordersArray));
    }

    /**
     * 新浪微博用户登录画面初始化
     */
    public function get_sinaSignin()
    {
        // sina用户对象初始化
        $sinaObj = new SaeTOAuthV2( '1864491839' , 'e433595a2f74b48b15713da46f490baa' );
        $code = Input::get('code');
        // token取得
        if (isset($code)) {
            $keys = array();
            $keys['code'] = $code;
            $keys['redirect_uri'] = 'http://www.tieegg.com/sinaSignin';
            try {
                $token = $sinaObj->getAccessToken( 'code', $keys ) ;
            } catch (OAuthException $e) {
                return Response::error('500');
            }
        } else {
            $view = View::make('account.sinasignup');
            $view->title = '注册蛋友 By 新浪微博用户';
            return $view;
        }

        // 取得的token设定到cookie中
        if (isset($token)) {
            Session::put('token', $token);
            setcookie( 'weibojs_'.$sinaObj->client_id, http_build_query($token) );
        } else {
            return Response::error('500');
        }

        // 取得返回信息
        $c = new SaeTClientV2( '1864491839' , 'e433595a2f74b48b15713da46f490baa' , Session::get('token.access_token') );
        $uid_get = $c->get_uid();
        // 用户ID
        $uid = $uid_get['uid'];
        // 根据ID获取用户等基本信息
        $user_info = $c->show_user_by_id( $uid );

        // 判断用户ID在数据库中是否存在
        $sinaUser = UserMeta::where('sina_id', '=', $uid)->first();

        try {

            // 存在时, 授权过 更新accesstoken
            if ($sinaUser) {
                $sysUser = Sentry::user((int)$sinaUser->user_id);
                $userData = array(
                    'metadata' => array(
                        'sina_access_token' => Session::get('token.access_token'),
                    ),
                );

                // 更新accesstoken
                if ($sysUser->update($userData)) {
                    // 新浪微博用户登录
                    if (Sentry::loginById((int)$sysUser->id)) {
                        // 购物车数据初始化
                        static::exchangeCartInfo();
                        // 主页迁移
                        return Redirect::to('/');
                    } else {
                        return Response::error('500');
                    }
                } else {
                    return Response::error('500');
                }

            } else { // 不存在时, 注册新用户然后登录

                // 整理注册用数据
                $vars = array(
                    'username' => sizeOf(User::where('username', '=', $user_info['screen_name'])->get()) > 0 ? $user_info['screen_name'] . 'Rd' . rand(10000, 99999) : $user_info['screen_name'],
                    'email'    => $uid,
                    'password' => 'sinaId' . $uid,
                    'metadata' => array(
                        'first_name' => 'first',
                        'last_name'  => 'last',
                        // 自定义字段添加在这里
                        'sex' => 0,
                        'baby_status' => 0,
                        'baby_age' => 0,
                        'baby_sex' => 0,
                        'sina_id' => $uid,
                        'sina_access_token' => Session::get('token.access_token'),
                        'social_type' => 1,
                    )
                );

                // 注册用户
                if ($user_id = Sentry::user()->create($vars))
                {
                    // 新浪微博用户登录
                    if (Sentry::loginById((int)$user_id)) {
                        // 购物车数据初始化
                        static::exchangeCartInfo();
                        // 主页迁移
                        return Redirect::to('/');
                    } else {
                        return Response::error('500');
                    }
                }
                else
                {
                    // 出错了,500画面迁移(正常不会到这里)
                    return Response::error('500');
                }
            }
        }
        catch (Sentry\SentryException $e)
        {
            $errors = $e->getMessage();
            return var_dump($errors);
        }
    }

    /**
     * QQ用户登录画面初始化
     */
    public function get_qqSignin()
    {
        $view = View::make('account.qqsignup');
        $view->title = '注册蛋友 By QQ用户';
        return $view;
    }

    /**
     * QQ用户登录画面初始化
     */
    public function post_qqSignin()
    {
        // QQ ID
        $uid = Input::get('qq_id');
        // QQ Access Token
        $access_token = Input::get('qq_access_token');

        // 基本QQ数据不正, 异常页面迁移
        if (!isset($uid) || !isset($access_token)) {
            // 出错了,500画面迁移(正常不会到这里)
            return Response::json(array('error' => true));
        }

        // 判断用户ID在数据库中是否存在
        $qqUser = UserMeta::where('qq_id', '=', $uid)->first();

        try {

            // 存在时, 授权过 更新accesstoken
            if ($qqUser) {
                $sysUser = Sentry::user((int)$qqUser->user_id);
                $userData = array(
                    'metadata' => array(
                        'qq_access_token' => $access_token,
                        'social_type' => 2,
                    ),
                );

                // 更新accesstoken
                if ($sysUser->update($userData)) {
                    // QQ用户登录
                    if (Sentry::loginById((int)$sysUser->id)) {
                        // 购物车数据初始化
                        static::exchangeCartInfo();
                        // 主页迁移
                        return Response::json(array('error' => false));
                    } else {
                        return Response::json(array('error' => true));
                    }
                } else {
                    return Response::json(array('error' => true));
                }

            } else { // 不存在时, 注册新用户然后登录

                // 整理注册用数据
                $vars = array(
                    'username' => sizeOf(User::where('username', '=', $uid)->get()) > 0 ? $uid . 'Rd' . rand(10000, 99999) : $uid,
                    'email'    => $uid,
                    'password' => 'qqId' . $uid,
                    'metadata' => array(
                        'first_name' => 'first',
                        'last_name'  => 'last',
                        // 自定义字段添加在这里
                        'sex' => 0,
                        'baby_status' => 0,
                        'baby_age' => 0,
                        'baby_sex' => 0,
                        'qq_id' => $uid,
                        'qq_access_token' => $access_token,
                        'social_type' => 2,
                    )
                );

                // 注册用户
                if ($user_id = Sentry::user()->create($vars))
                {
                    // 新浪微博用户登录
                    if (Sentry::loginById((int)$user_id)) {
                        // 购物车数据初始化
                        static::exchangeCartInfo();
                        // 主页迁移
                        return Response::json(array('error' => false));
                    } else {
                        return Response::json(array('error' => true));
                    }
                }
                else
                {
                    // 出错了,500画面迁移(正常不会到这里)
                    return Response::json(array('error' => true));
                }
            }
        }
        catch (Sentry\SentryException $e)
        {
            return Response::json(array('error' => true));
        }
    }

    /**
     * 用户购物车信息初始化
     */
    public static function exchangeCartInfo() {

        // 当前登录用户
        $user = Sentry::user();

        // 当前登录用户的购物车信息
        $shopCartInfos = ShopCarts::where('user_id', '=', $user->id)->get();

        // 数据库中存在用户购物车数据的时候
        if (sizeOf($shopCartInfos) > 0) {

            // 初始化用户购物车
            foreach ($shopCartInfos as $obj) {
                $product_id = $obj->product_id;
                $qty = $obj->quantity;
                $product = Product::find($product_id);
                $item = array(
                    'id'      => $product_id,
                    'qty'     => $qty,
                    'price'   => $product->coupon_price,
                    'name'    => $product->product_name,
                    'image'   => $product->product_img_url,
                    'options' => array()
                );
                Cartify::cart('user_cart_' . $user->id)->insert($item);
            }
        } else {
            // 临时购物车不是空的
            if (Cartify::cart()->total()) {
                $cart_contents = Cartify::cart()->contents();
                foreach ($cart_contents as $cart_item) {
                    // 初始化用户购物车
                    Cartify::cart('user_cart_' . $user->id)->insert($cart_item);
                    // 将临时购物车数据登录到DB中
                    $shopCartObj = new ShopCarts;
                    // 用户ID
                    $shopCartObj->user_id = $user->id;
                    // 货品ID
                    $shopCartObj->product_id = $cart_item['id'];
                    // 货品购买数量
                    $shopCartObj->quantity = $cart_item['qty'];
                    // 插入数据
                    $shopCartObj->save();
                }
            }
        }

        // 清空临时购物车
        Cartify::cart()->destroy();

    }

}

?>
