@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        {{ Form::open('signin', 'POST', array('class' => 'form-horizontal')) }}
            <div>

                <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                    <div class="pull-left" style="margin-left:10px;">
                        <h4><b>欢迎“蛋友”</b></h4>
                    </div>
                </div>

                <div class="row-fluid" style="margin-top: 50px; margin-bottom: 50px;">

                    <div class="span6">
                        @if (Session::has('error_flg'))
                        <div class="control-group">
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <span class="text-primary">{{ Session::get('error_msg') }}</span>
                            </div>
                        </div>
                        @endif

                        <div class="control-group">
                            {{ Form::label('email', 'E-mail', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('email', $email, array('class' => 'login-field', 'placeholder' => 'E-MAIL', 'id' => 'email')) }}
                            </div>
                        </div>

                        <div class="control-group">
                            {{ Form::label('password', '密码', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::password('password', array('class' => 'login-field', 'placeholder' => 'PASSWORD', 'id' => 'password')) }}
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <label class="checkbox" for="remember">
                                    <input type="checkbox" value="1" id="remember" name="remember" checked="checked" data-toggle="checkbox">
                                    两个星期内免登录
                                </label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                {{ Form::submit('登入', array('id' => 'loginBtn', 'class' => 'btn btn-primary btn-large btn-embossed')) }}
                                {{ HTML::link('/signup', '注册铁蛋壳', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-large btn-embossed', 'style' => 'margin-left: 10px;')) }}
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="row-fluid">

                        </div>

                        <div class="row-fluid">
                            <div class="span12" style="text-align: center;">
                                <h4>社交帐号登录铁蛋壳</h4>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span5 offset1" style="text-align: center;margin-bottom: 10px;">
                                <span id="sinaLoginBtn"><a href="{{ $code_url }}">{{ HTML::image('/img/openImage/weibo_login.png', '点击进入授权页面', array('style' => '')) }}</a></span>
                            </div>

                            <div class="span5" style="text-align: center;margin-bottom: 10px;">
                                <span id="qqLoginBtn"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        {{ Form::close() }}
    </div>
@endsection

@section("footer")
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        QC.Login({
           btnId:"qqLoginBtn",   //QQ登录按钮的节点id
           size: "A_XL",
        });
        var paras = {};
        QC.api("get_info", paras)
            //指定接口访问成功的接收函数，s为成功返回Response对象
            .success(function(s){
                //$('#qqname').val(s.data.data.name);
                QC.Login.getMe(function(openId, accessToken){
                    $.ajax({
                       type: "POST",
                       url: "/qqSignin",
                       datatype: 'json',
                       async: false,
                       data: {
                           'qq_id': s.data.data.name,
                           'qq_access_token': accessToken
                       },
                       success: function(msg){
                        if (!msg.error) {
                            window.location.href = '/';
                        }
                       },
                       error: function(e){
                       }
                    });
                });

            })
            //指定接口访问失败的接收函数，f为失败返回Response对象
            .error(function(f){
                //失败回调
            })
            //指定接口完成请求后的接收函数，c为完成请求返回Response对象
            .complete(function(c){
                //完成请求回调
            });
    </script>
@endsection
