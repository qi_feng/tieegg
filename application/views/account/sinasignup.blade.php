@layout('base')

@section('headerScript')
    @parent
    <script type="text/javascript">
        $(function(){
        });
        function changeCaptcha() {
            $("#captchaImg").attr('src', '{{ CoolCaptcha\Captcha::img() }}' );
        }
    </script>
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        {{ Form::open('sinaSignin', 'POST', array('class' => 'form-horizontal', 'data-validate' => 'parsley')) }}
            <fieldset>
                <div class="row-fluid detail-hr">
                    <div class="span4 pull-left muted">
                        <h4><b>新浪微博用户->注册铁蛋壳</b></h4>
                    </div>
                </div>
                @if (Session::has('error_flg'))
                <div class="control-group">
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ Session::get('error_msg') }}
                    </div>
                </div>
                @endif
                <div class="control-group {{ $errors->has('nickname') ? 'error' : '' }}">
                    {{ Form::label('nickname', '昵称', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('nickname', Input::old('nickname') ? Input::old('nickname') : $sinaName, array('class' => 'login-field input-large', 'placeholder' => '例:a123_tieegg', 'id' => 'nickname', 'data-trigger' => 'keyup', 'data-error-container' => '#nicknameErrSpan', 'data-required' => 'true', 'data-rangelength' => '[4,24]', 'data-regexp' => '^[0-9a-zA-Z\u4e00-\u9fa5_-]+$')) }}
                        <span id="nicknameErrSpan" class="help-inline">
                            @if ($errors->has('nickname'))
                            <div class="alert alert-error" style="margin:auto;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $errors->first('nickname') }}
                            </div>
                            @endif
                        </span>
                        <span class="help-block"><small class="text-warning">*可以是中文,字母,数字,下划线!长度4~24,中文占2个字符.</small></span>
                    </div>
                </div>
                <div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
                    {{ Form::label('email', 'E-Mail', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('email', Input::old('email'), array('class' => 'login-field input-large', 'placeholder' => '例:sample@tieegg.com', 'id' => 'email', 'data-type' => 'email', 'data-trigger' => 'keyup', 'data-error-container' => '#emailErrSpan', 'data-required' => 'true', 'data-maxlength' => '40')) }}
                        <span id="emailErrSpan" class="help-inline">
                            @if ($errors->has('email'))
                            <div class="alert alert-error" style="margin:auto;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </span>
                    </div>
                </div>
                <div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
                    {{ Form::label('password', '密码', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::password('password', array('class' => 'login-field input-large', 'placeholder' => '设置密码', 'id' => 'password', 'data-trigger' => 'keyup', 'data-error-container' => '#passwordErrSpan', 'data-required' => 'true', 'data-rangelength' => '[6,18]')) }}
                        <span id="passwordErrSpan" class="help-inline">
                            @if ($errors->has('password'))
                            <div class="alert alert-error" style="margin:auto;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </span>
                        <span class="help-block"><small class="text-warning">*任意字符,长度6~18.</small></span>
                    </div>
                </div>
                <div class="control-group {{ $errors->has('repassword') ? 'error' : '' }}">
                    {{ Form::label('repassword', '再输入一次密码', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::password('repassword', array('class' => 'login-field input-large', 'placeholder' => '确认密码', 'id' => 'repassword', 'data-trigger' => 'keyup', 'data-error-container' => '#repasswordErrSpan', 'data-required' => 'true', 'data-equalto' => '#password', 'data-equalto-message' => '两次输入的密码不一致, 请重新输入!')) }}
                        <span id="repasswordErrSpan" class="help-inline">
                            @if ($errors->has('repassword'))
                            <div class="alert alert-error" style="margin:auto;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $errors->first('repassword') }}
                            </div>
                            @endif
                        </span>
                    </div>
                </div>

                <div class="control-group {{ $errors->has('captcha') ? 'error' : '' }}">
                    {{ Form::label('captcha', '验证码', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('captcha', Input::old('captcha'), array('class' => 'login-field input-large', 'placeholder' => '验证码', 'id' => 'captcha', 'data-trigger' => 'keyup', 'data-error-container' => '#captchaErrSpan', 'data-required' => 'true')) }}
                        <span id="captchaErrSpan" class="help-inline">
                            @if ($errors->has('captcha'))
                            <div class="alert alert-error" style="margin:auto;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $errors->first('captcha') }}
                            </div>
                            @endif
                        </span>
                        <p>
                            {{ HTML::image(CoolCaptcha\Captcha::img(), 'captcha', array('class' => 'captchaimg', 'id' => 'captchaImg')) }}
                            {{ HTML::link('#', '看不清楚?换一换.', array('id' => 'change-image', 'onclick' => 'changeCaptcha();')) }}
                        </p>
                    </div>
                </div>

                <div class="form-actions">
                    <span class="span2">
                        {{ Form::submit('注册', array('id' => 'signupBtn', 'class' => 'btn btn-primary btn-large btn-block')) }}
                    </span>
                    <span class="span2 mtm">
                        {{ HTML::link('/signin', '我是蛋友, 这里登入', array('id' => 'signinLink', 'class' => 'login-link')) }}
                    </span>
                </div>
            </fieldset>
        {{ Form::close() }}
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
@endsection
