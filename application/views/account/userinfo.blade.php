@layout('base')

@section('headerScript')
    @parent
    <script type="text/javascript">
    </script>
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
            <fieldset>
                <div class="row-fluid" style="margin-bottom: 0px;">
                    <div class="span10 pull-left muted">
                        <h4><b>我的蛋壳</b></h4>
                    </div>
                    <div class="span2 pull-right">
                        {{ HTML::link('/signout', '退出登录', array('id' => 'signoutLink', 'class' => 'btn btn-small btn-block btn-embossed')) }}
                    </div>
                </div>
                @if (Session::has('error_flg'))
                <div class="control-group">
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span class="text-primary">{{ Session::get('error_msg') }}</span>
                    </div>
                </div>
                @endif
                @if (Session::has('success_flg'))
                <div class="control-group">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ Session::get('success_msg') }}
                    </div>
                </div>
                @endif
                @if (Session::has('warning_flg'))
                <div class="control-group">
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ Session::get('warning_msg') }}
                    </div>
                </div>
                @endif
                <br>
                <ul class="nav nav-tabs nav-append-content" id="userInfoTab">
                    <li><a href="#userInfo" data-toggle="tab">蛋友资料</a></li>
                    @if ($user->get('metadata.social_type') == 0)
                        <li><a href="#passChange" data-toggle="tab">修改密码</a></li>
                    @endif
                    <li><a href="#deliveryAddr" data-toggle="tab">送货地址</a></li>
                    <li><a href="#mytrade" data-toggle="tab">我的订单</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="userInfo">
                        {{ Form::open('userinfo', 'POST', array('class' => 'form-horizontal', 'data-validate' => 'parsley')) }}
                            @if ($user->get('metadata.social_type') == 0)
                            <div class="control-group">
                                {{ Form::label('', 'E-Mail', array('class' => 'control-label')) }}
                                <div class="controls">
                                    <span class="help-inline" style="margin-top:6px;">
                                        <p class="text-info">{{ $user->email }}</p>
                                    </span>
                                </div>
                            </div>
                            @endif
                            <div class="control-group {{ $errors->has('nickname') ? 'error' : '' }}">
                                {{ Form::label('nickname', '昵称', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::text('nickname', Input::old('nickname') ? Input::old('nickname') : $user->username, array('class' => 'login-field input-large', 'placeholder' => '例:a123_tieegg', 'id' => 'nickname', 'data-trigger' => 'keyup', 'data-error-container' => '#nicknameErrSpan', 'data-required' => 'true', 'data-rangelength' => '[4,24]', 'data-regexp' => '^[0-9a-zA-Z\u4e00-\u9fa5_-]+$')) }}
                                    <span id="nicknameErrSpan" class="help-inline">
                                        @if ($errors->has('nickname'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('nickname') }}
                                        </div>
                                        @endif
                                    </span>
                                    <span class="help-block"><small class="text-warning">*可以是中文,字母,数字,下划线!长度4~24,中文占2个字符.</small></span>
                                </div>
                            </div>
                            <div class="control-group {{ $errors->has('sex') ? 'error' : '' }}">
                                {{ Form::label('sex', '性别', array('class' => 'control-label')) }}
                                <div class="controls">
                                    <div style="width:200px;">
                                        {{ Form::select('sex', $sexAry, Input::old('sex') ? Input::old('sex') : $user->get('metadata.sex'), array('id' => 'sex', 'class' => 'select-block info')) }}
                                    </div>
                                    <span id="sexErrSpan" class="help-inline">
                                        @if ($errors->has('sex'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('sex') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="control-group {{ $errors->has('babyStatus') ? 'error' : '' }}">
                                {{ Form::label('babyStatus', '状态', array('class' => 'control-label')) }}
                                <div class="controls">
                                    <div style="width:200px;">
                                        {{ Form::select('babyStatus', $babyStatusAry, Input::old('babyStatus') ? Input::old('babyStatus') : $user->get('metadata.baby_status'), array('id' => 'babyStatus', 'class' => 'select-block info')) }}
                                    </div>
                                    <span id="babyStatusErrSpan" class="help-inline">
                                        @if ($errors->has('babyStatus'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('babyStatus') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="control-group {{ $errors->has('babyAge') ? 'error' : '' }}">
                                {{ Form::label('babyAge', '宝宝年龄', array('class' => 'control-label')) }}
                                <div class="controls">
                                    <div style="width:200px;">
                                        {{ Form::select('babyAge', $babyAgeAry, Input::old('babyAge') ? Input::old('babyAge') : $user->get('metadata.baby_age'), array('id' => 'babyAge', 'class' => 'select-block info')) }}
                                    </div>
                                    <span id="babyAgeErrSpan" class="help-inline">
                                        @if ($errors->has('babyAge'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('babyAge') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="control-group {{ $errors->has('babySex') ? 'error' : '' }}">
                                {{ Form::label('babySex', '宝宝性别', array('class' => 'control-label')) }}
                                <div class="controls">
                                    <div style="width:200px;">
                                        {{ Form::select('babySex', $babySexAry, Input::old('babySex') ? Input::old('babySex') : $user->get('metadata.baby_sex'), array('id' => 'babySex', 'class' => 'select-block info')) }}
                                    </div>
                                    <span id="babySexErrSpan" class="help-inline">
                                        @if ($errors->has('babySex'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('babySex') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="form-actions">
                                <span class="pull-right span2">
                                    {{ Form::submit('修改资料', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-block btn-embossed')) }}
                                </span>
                            </div>
                        {{ Form::close() }}
                    </div>
                    @if ($user->get('metadata.social_type') == 0)
                    <div class="tab-pane" id="passChange">
                        {{ Form::open('passchange', 'POST', array('class' => 'form-horizontal', 'data-validate' => 'parsley')) }}
                            <div class="control-group {{ $errors->has('prepassword') ? 'error' : '' }}">
                                {{ Form::label('prepassword', '密码', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::password('prepassword', array('class' => 'login-field input-large', 'placeholder' => '原密码', 'id' => 'prepassword', 'data-trigger' => 'keyup', 'data-error-container' => '#prepasswordErrSpan', 'data-required' => 'true')) }}
                                    <span id="prepasswordErrSpan" class="help-inline">
                                        @if ($errors->has('prepassword'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('prepassword') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="control-group {{ $errors->has('newpassword') ? 'error' : '' }}">
                                {{ Form::label('newpassword', '新密码', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::password('newpassword', array('class' => 'login-field input-large', 'placeholder' => '设置密码', 'id' => 'newpassword', 'data-trigger' => 'keyup', 'data-error-container' => '#newpasswordErrSpan', 'data-required' => 'true', 'data-rangelength' => '[6,18]')) }}
                                    <span id="newpasswordErrSpan" class="help-inline">
                                        @if ($errors->has('newpassword'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('newpassword') }}
                                        </div>
                                        @endif
                                    </span>
                                    <span class="help-block"><small class="text-warning">*任意字符,长度6~18.</small></span>
                                </div>
                            </div>
                            <div class="control-group {{ $errors->has('repassword') ? 'error' : '' }}">
                                {{ Form::label('repassword', '再输入一次新密码', array('class' => 'control-label')) }}
                                <div class="controls">
                                    {{ Form::password('repassword', array('class' => 'login-field input-large', 'placeholder' => '确认密码', 'id' => 'repassword', 'data-trigger' => 'keyup', 'data-error-container' => '#repasswordErrSpan', 'data-required' => 'true', 'data-equalto' => '#newpassword', 'data-equalto-message' => '两次输入的密码不一致, 请重新输入!')) }}
                                    <span id="repasswordErrSpan" class="help-inline">
                                        @if ($errors->has('repassword'))
                                        <div class="alert alert-error" style="margin:auto;">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $errors->first('repassword') }}
                                        </div>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <span class="pull-right span2">
                                    {{ Form::submit('修改密码', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-embossed btn-block')) }}
                                </span>
                            </div>
                        {{ Form::close() }}
                    </div>
                    @endif
                    <div class="tab-pane" id="deliveryAddr">
                        <div class="control-group">
                            @if (count($deliveryAddrs) > 0)
                                <table class="table table-bordered table-hover table-striped">
                                    <thead style="background-color:#ECF0F1;">
                                        <th>操作</th>
                                        <th>收货人</th>
                                        <th>收货地址</th>
                                        <th>联系电话</th>
                                    </thead>
                                    <tbody>
                                    @for ($i = 0; $i < count($deliveryAddrs); $i++)
                                        @if ($deliveryAddrs[$i]->main_addr == 1)
                                        <tr class="info">
                                        @else
                                        <tr>
                                        @endif
                                            <td>
                                                <a href="#" onclick="setDefaultAddr({{ $deliveryAddrs[$i]->id }});">默认</a>
                                                <a href="#" onclick="deleteAddr({{ $deliveryAddrs[$i]->id }});">删除</a>
                                            </td>
                                            <td>{{ $deliveryAddrs[$i]->name }}</td>
                                            <td>{{ $deliveryAddrs[$i]->province }} {{ $deliveryAddrs[$i]->city }} {{ $deliveryAddrs[$i]->area }} {{ $deliveryAddrs[$i]->address }}</td>
                                            <td>{{ $deliveryAddrs[$i]->contact_phone }}</td>
                                        </tr>
                                    @endfor
                                        <tr>
                                            <td colspan="4" style="background-color:#ECF0F1;">
                                                <span class="pull-right label label-info">*蓝色标记为当前默认地址.</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @else
                                <div class="alert"><strong>嘿!</strong> 你还没有添加送货地址哦!</div>
                            @endif
                        </div>
                        <div class="well">
                            {{ Form::open('deliveryadd', 'POST', array('class' => 'form-horizontal', 'data-validate' => 'parsley')) }}
                                <div class="row-fluid detail-hr" style="background-color: #f5f5f5;">
                                    <div class="span6 pull-left">
                                        <h6><b>编辑地址</b></h6>
                                    </div>
                                    <div class="span1 pull-right">
                                        {{ Form::submit('保存', array('id' => 'signupBtn', 'class' => 'btn btn-small btn-danger btn-embossed')) }}
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('guestname') ? 'error' : '' }}" style="margin-top:20px;">
                                    {{ Form::label('guestname', '收货人', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('guestname', Input::old('guestname'), array('class' => 'login-field input-large', 'placeholder' => '收货人', 'id' => 'guestname', 'data-trigger' => 'keyup', 'data-error-container' => '#guestnameErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                        <span id="guestnameErrSpan" class="help-inline">
                                            @if ($errors->has('guestname'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('guestname') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('province') ? 'error' : '' }}">
                                    {{ Form::label('province', '省', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('province', Input::old('province'), array('class' => 'login-field input-large', 'placeholder' => '例:辽宁省', 'id' => 'province', 'data-trigger' => 'keyup', 'data-error-container' => '#provinceErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                        <span id="provinceErrSpan" class="help-inline">
                                            @if ($errors->has('province'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('province') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('city') ? 'error' : '' }}">
                                    {{ Form::label('city', '市', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('city', Input::old('city'), array('class' => 'login-field input-large', 'placeholder' => '例:大连市', 'id' => 'city', 'data-trigger' => 'keyup', 'data-error-container' => '#cityErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                        <span id="cityErrSpan" class="help-inline">
                                            @if ($errors->has('city'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('city') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('area') ? 'error' : '' }}">
                                    {{ Form::label('area', '区,县', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('area', Input::old('area'), array('class' => 'login-field input-large', 'placeholder' => '例:甘井子区', 'id' => 'area', 'data-trigger' => 'keyup', 'data-error-container' => '#areaErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                        <span id="areaErrSpan" class="help-inline">
                                            @if ($errors->has('area'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('area') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('address') ? 'error' : '' }}">
                                    {{ Form::label('address', '街道,小区,门牌', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('address', Input::old('address'), array('class' => 'login-field input-large span6', 'placeholder' => '例:**街*号****小区**栋**楼**号', 'id' => 'address', 'data-trigger' => 'keyup', 'data-error-container' => '#addressErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,100]')) }}
                                        <span id="addressErrSpan" class="help-inline">
                                            @if ($errors->has('address'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('address') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('postcode') ? 'error' : '' }}">
                                    {{ Form::label('postcode', '邮政编码', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('postcode', Input::old('postcode'), array('class' => 'login-field input-large', 'placeholder' => '例:116023', 'id' => 'postcode', 'data-trigger' => 'keyup', 'data-error-container' => '#postcodeErrSpan', 'data-required' => 'true', 'data-minlength' => '6', 'data-maxlength' => '6')) }}
                                        <span id="postcodeErrSpan" class="help-inline">
                                            @if ($errors->has('postcode'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('postcode') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group {{ $errors->has('contactphone') ? 'error' : '' }}">
                                    {{ Form::label('contactphone', '联系电话', array('class' => 'control-label')) }}
                                    <div class="controls">
                                        {{ Form::text('contactphone', Input::old('contactphone'), array('class' => 'login-field input-large span4', 'placeholder' => '例:手机:13112341234/座机:0411-12345678', 'id' => 'contactphone', 'data-trigger' => 'keyup', 'data-error-container' => '#contactphoneErrSpan', 'data-required' => 'true', 'data-minlength' => '7', 'data-maxlength' => '16')) }}
                                        <span id="contactphoneErrSpan" class="help-inline">
                                            @if ($errors->has('contactphone'))
                                            <div class="alert alert-error" style="margin:auto;">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                {{ $errors->first('contactphone') }}
                                            </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <div class="tab-pane" id="mytrade">
                        <div class="control-group">
                            @if (count($trades) > 0)
                                <table class="table table-bordered table-hover table-striped">
                                    <thead style="background-color:#ECF0F1;">
                                        <th>订单号</th>
                                        <th>下单日期</th>
                                        <th>订单状态</th>
                                    </thead>
                                    <tbody>
                                    @for ($i = 0; $i < count($trades); $i++)
                                        <tr>
                                            <td><a href="#trade-detail" id="tradeNoLink{{ $i }}" class="text-primary" onclick="tradeLinkClick({{ $trades[$i]->my_trade_no }});">{{ $trades[$i]->my_trade_no }}</a></td>
                                            <td><span class="muted">{{ $trades[$i]->created_at }}</span></td>
                                            @if ($trades[$i]->trade_status == 1)
                                            <td><span class="label label-important label-large">等待用户付款</span></td>
                                            @elseif ($trades[$i]->trade_status == 2)
                                            <td><span class="label label-warning label-large">备货中</span></td>
                                            @elseif ($trades[$i]->trade_status == 3)
                                            <td><span class="label label-warning label-large">已发货</span></td>
                                            @elseif ($trades[$i]->trade_status == 4)
                                            <td><span class="label label-warning label-large">交易成功</span></td>
                                            @elseif ($trades[$i]->trade_status == 5)
                                            <td><span class="label label-warning label-large">交易异常</span></td>
                                            @endif
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            @else
                                <div class="alert"><strong>嘿!</strong> 没有找到和您关联的订单信息!</div>
                            @endif
                        </div>
                        <div class="demo-popovers" id="trade-detail">
                            <div class="popover bottom" style="margin:0px;">
                                <div class="arrow"></div>
                                <h3 class="popover-title" id="tradeNoTitle"></h3>
                                <div class="popover-content">
                                    <div class="pull-right">共计<span class="label label-important mlm" id="totalPrice"></span></div>
                                    <div class="detail-hr"><span class="text-danger"><b>货品信息:</b></span></div>
                                    <ul class="mts" id="orderList"></ul>
                                    <div class="detail-hr"><span class="text-danger"><b>收货信息:</b></span></div>
                                    <div id="deliveryInfo" class="mts"></div>
                                    <div class="detail-hr mtm"><span class="text-danger"><b>支付信息:</b></span></div>
                                    <div id="payInfo" class="mts mbl"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        (function() {
            var tabHrefstr = String('{{ Session::get('tabHref') }}');
            if (tabHrefstr == '' || tabHrefstr == null || tabHrefstr == undefined) {
                tabHrefstr = "#userInfo";
            }
            $("#userInfoTab a[href='" + tabHrefstr + "']").tab('show');

            @if ($user->get('metadata.social_type') == 2)
            $('#signoutLink').attr('onclick', 'QC.Login.signOut();');
            @endif

            $('#trade-detail').hide();
        })();

        function setDefaultAddr(addrId) {
            $.ajax({
                type: "POST",
                url: "/set_default_addr",
                datatype: 'json',
                async: true,
                data: {
                    'addrId': addrId
                },
                success: function(msg){
                    window.location.href = '/userinfo';
                },
                error: function(e){
                }
            });
        }

        function deleteAddr(addrId) {
            $.ajax({
                type: "POST",
                url: "/remove_addr",
                datatype: 'json',
                async: true,
                data: {
                    'addrId': addrId
                },
                success: function(msg){
                    window.location.href = '/userinfo';
                },
                error: function(e){
                }
            });
        }

        function tradeLinkClick(tradeNumber) {
            var tradeNo = tradeNumber;
            $.ajax({
                type: "POST",
                url: "/mytrade",
                datatype: 'json',
                async: true,
                data: {
                    'trade_no': tradeNo
                },
                success: function(msg){
                    if (!msg.error) {
                        var tradeInfo = msg.tradeInfo;
                        var ordersInfo = msg.ordersInfo;
                        $('#trade-detail').show();
                        $('#tradeNoTitle').html('订单[' + tradeInfo.my_trade_no + ']');
                        var detailHtml = '';
                        for (var i=0;i<ordersInfo.length;i++) {
                            detailHtml = detailHtml + '<li><a class="mbs" href="/show-product/' + ordersInfo[i].product_id + '" target="_blank">' + ordersInfo[i].product_name + '<span class="badge badge-info mll">&yen;' + ordersInfo[i].sell_price + ' * ' + ordersInfo[i].quantity + '</span></a></li>';
                        }
                        $('#orderList').html(detailHtml);
                        $('#totalPrice').html('&yen;' + tradeInfo.total_price);

                        var deliveryInfo = '';
                        deliveryInfo = deliveryInfo + '<span class="mrm mll">' + tradeInfo.delivery_name + '</span>';
                        deliveryInfo = deliveryInfo + '<span class="mrm">' + tradeInfo.delivery_address + '</span>';
                        deliveryInfo = deliveryInfo + '<span class="mrm">' + tradeInfo.delivery_phone + '</span>';
                        $('#deliveryInfo').html(deliveryInfo);

                        var payInfo = '';
                        if (tradeInfo.pay_type == 1) {
                            payInfo = payInfo + '<span class="mll mrl">在线支付</span>';
                        } else {
                            payInfo = payInfo + '<span class="mll mrl">货到付款</span>';
                        }
                        payInfo = payInfo + '运费<span class="label label-important mlm">&yen;' + tradeInfo.delivery_price + '</span>';
                        $('#payInfo').html(payInfo);
                    }
                },
                error: function(e){
                }
            });
        }
    </script>
@endsection
