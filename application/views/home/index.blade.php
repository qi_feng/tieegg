@layout('base')

<!-- 页头引用 -->
@section('headerScript')
    {{ HTML::style('plugins/coolKitten/css/grid.css') }}
    {{ HTML::style('plugins/coolKitten/css/style.css') }}
    {{ HTML::style('plugins/coolKitten/css/normalize.css') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,700,600') }}
    {{ HTML::style('css/main.css') }}
@endsection

@section('header')
    <div class="menu">
        <div class="container clearfix" >
            <div id="logo" class="grid_3">
                <a href="/">{{ HTML::image('img/tieegg-logo.png') }}</a>
            </div>

            <div id="nav" class="grid_9 omega">
                <ul class="navigation">
                    <li data-slide="1">
                        <i class="icon-home"></i>主页
                    </li>
                    <li data-slide="2">
                        <i class="icon-tags"></i>蛋壳东西
                    </li>
                    <li data-slide="3">
                        <i class="icon-truck"></i>送货方式
                    </li>
                    <li data-slide="4">
                        <i class="icon-info-sign"></i>关于我们
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection

<!-- 内容 -->
@section('content')

    <div class="slide" id="slide1" data-slide="1" data-stellar-background-ratio="1">
        <div class="container clearfix">

            <div id="content" class="grid_7">
                <h1 style="color: #fc6651;">足不出户 畅享购物</h1>
                <h2><sup class="icon-quote-left"></sup> 生活始于宝贝的世界 <sup class="icon-quote-right"></sup></h2>
                <p>
                    <font style="padding:5px;">蛋爸 & 蛋妈，马上行动起来Join铁蛋壳</font>
                </p>
                <p>
                    @if (Sentry::check() == true)
                        <span class="button-red button-1">
                            <a href="userinfo"><i class="icon-user"></i> 我的蛋壳</a>
                        </span>
                        <span class="button-blue button-1">
                            <a href="signout" id="signoutTopLink"><i class="icon-signout"></i> 登出蛋友</a>
                        </span>
                    @else
                        <span class="button-red button-1">
                            <a href="signup"><i class="icon-user"></i> 注册蛋友</a>
                        </span>
                        <span class="button-blue button-1">
                            <a href="signin"><i class="icon-signin"></i> 登入蛋友</a>
                        </span>
                    @endif
                </p>
                <h5 style="margin-bottom:0;"><i class="icon-qrcode"></i> 扫一扫二维码, 用手机浏览我们</h5>
                {{ HTML::image('img/QRTieegg.png', '请扫二维码', array('style' => 'width:25%;margin-top:0px;')) }}
            </div>
            <div id="decorative" class="grid_5 omega">
            </div>
        </div>
    </div>

    <div class="slide" id="slide2" data-slide="2" data-stellar-background-ratio="1">
        <div class="container clearfix">
            <div class="tooltip-arrow-green tooltip-green" style=""></div>

            <div id="content" class="grid_6 t-center">
                <h2>淘"蛋壳"东西</h2>
                <p class="index-slide-img">
                    <a href="/store-categorys/usage">{{ HTML::image('img/categorys/bag.png') }}</a>
                </p>
                <span class="button-red button-1">
                    <a href="/store-categorys/usage"><i class="icon-gift"></i> 类别SHOPPING</a>
                </span>
                <h3>想给宝宝买什么？</h3>
            </div>

            <div id="content" class="grid_6 omega t-center">
                <h2>"蛋仔"现在这么大</h2>
                <p class="index-slide-img">
                    <a href="/store-categorys/age">{{ HTML::image('images/icons/Calendar@2x.png') }}</a>
                </p>
                <span class="button-yellow button-1">
                    <a href="/store-categorys/age"><i class="icon-time"></i> 月龄SHOPPING</a>
                </span>
                <h3>宝宝现在该用什么？</h3>
            </div>

        </div>
        <!-- <div id="cbp-fwslider" class="cbp-fwslider">
            <ul>
                <li><a href="#"><img src="plugins/fullImageSlider/images/1.jpg" alt="img01"/></a></li>
                <li><a href="#"><img src="plugins/fullImageSlider/images/2.jpg" alt="img02"/></a></li>
                <li><a href="#"><img src="plugins/fullImageSlider/images/3.jpg" alt="img03"/></a></li>
                <li><a href="#"><img src="plugins/fullImageSlider/images/4.jpg" alt="img04"/></a></li>
                <li><a href="#"><img src="plugins/fullImageSlider/images/5.jpg" alt="img05"/></a></li>
            </ul>
        </div> -->
    </div>

    <div class="slide" id="slide3" data-slide="3" data-stellar-background-ratio="1">
        <div class="container clearfix">
            <div class="tooltip-arrow-red tooltip-red" style=""></div>

            <div class="grid_8">
                <div class="t-right" style="margin-top: 5em;">
                    <h2>“货到付款”</h2>
                    <p><h3>提供货到付款服务，验货满意后再付款，购物放心。</h3></p>
                </div>
            </div>

            <div class="grid_4 omega t-center">
                <p class="tile-slide-img">
                    {{ HTML::image('images/icons/Card@2x.png') }}
                </p>
            </div>

            <div class="grid_4 t-center">
                <p class="tile-slide-img">
                    {{ HTML::image('img/categorys/map.png') }}
                </p>
            </div>

            <div class="grid_8 omega">
                <div class="t-left" style="margin-top: 5em;">
                    <h2>“送货区域”</h2>
                    <p><h3>支持高新园区&软件园免费送货，大连市内城区满50元免快递费。</h3></p>
                </div>
            </div>

        </div>
    </div>

@endsection
<!-- 页脚 -->
@section('footer')
    <div class="slide" id="slide4" data-slide="4" data-stellar-background-ratio="1">
        <div class="container clearfix">
            <div class="tooltip-arrow-blue tooltip-blue" style=""></div>
            <div id="content" class="grid_12">
                <h2>为每个家庭，提供不一样的购物体验</h2>
                <h4><i class="icon-envelope-alt"></i> 联系我们：{{ HTML::mailto('tieegg.service@gmail.com', 'tieegg.service@gmail.com') }}</h4>
                <p>
                    <i class="icon-eye-open"></i> 关注我们 :
                    <!-- JiaThis Button BEGIN -->
                    <div class="jiathis_style_32x32">
                        <a class="jiathis_follow_tsina" rel="http://weibo.com/u/3357659360"></a>
                        <a class="jiathis_follow_weixin" rel="img/qrcode_for_tiedanke.jpg"></a>
                    </div>
                    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1353836035227557" charset="utf-8"></script>
                    <!-- JiaThis Button END -->
                </p>
            </div>
            <h6 style="color: gray;"><a href="http://www.miitbeian.gov.cn/" target=_blank>@2013 辽ICP备13004915号</a></h6>
        </div>
    </div>
@endsection
<!-- 页脚下引用 -->
@section("footerScript")
    {{ HTML::script('plugins/coolKitten/js/js.js') }}
    {{ HTML::script('plugins/coolKitten/js/jquery.stellar.min.js') }}
    {{ HTML::script('plugins/coolKitten/js/waypoints.min.js') }}
    {{ HTML::script('plugins/coolKitten/js/jquery.easing.1.3.js') }}
    {{ HTML::script('plugins/fullImageSlider/js/jquery.cbpFWSlider.min.js') }}
    <script>
        (function() {
            $( '#cbp-fwslider' ).cbpFWSlider();
            @if (Sentry::check() && Sentry::user()->get('metadata.social_type') == 2)
            $('#signoutTopLink').click(function(){
                QC.Login.signOut();
            });
            @endif
        })();
    </script>
@endsection
