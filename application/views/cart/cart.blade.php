@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
<div class="container-narrow">

	@if ($success = Session::get('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>噹噹!</strong> {{ $success }}
		</div>
	@endif
	@if ($error = Session::get('error'))
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<span class="text-primary"><strong>啊欧!</strong> {{ $error }}</span>
		</div>
	@endif
	@if ($warning = Session::get('warning'))
	<div class="alert alert-warning">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>叮咚!</strong> {{ $warning }}
	</div>
	@endif

	<form method="post" action="cart" class="form-horizontal">
		<div class="muted"><h4><b>我的蛋购袋</b></h4></div>
		<table class="table table-hover table-striped table-bordered">
			<thead>
				<tr style="background-color:#3498DB;">
					<th width="6%"></th>
					<th width="40%"><span style="color: #ECF0F1;">货品名</span></th>
					<th width="8%"><span style="color: #ECF0F1;">数量</span></th>
					<th width="12%"><span style="color: #ECF0F1;">单价</span></th>
					<th width="12%"><span style="color: #ECF0F1;">共计</span></th>
				</tr>
			</thead>
			<tbody>
				@forelse ($cart_contents as $item)
				<!-- Get the product options, you should get product related options on your controller! -->
				<?php $product_options = Cartify\Models\Products::get_options($item['id']); ?>

				<tr>
					<td class="align-center">
						<span class="span1 thumbnail mlm mrm"><img src="{{ URL::to_asset('img' . $item['image']) }}" /></span>
					</td>
					<td>
						<strong><a href="{{ URL::to('show-product/' . $item['id']) }}" target="_blank">{{ $item['name'] }}</a></strong>

						<span class="pull-right">
							<a href="{{ '/remove-product/' . $item['rowid'] }}" rel="tooltip" title="删除货品" class="btn btn-mini btn-embossed"><i class="icon icon-white icon-remove"></i></a>
						</span>

						<!-- Check if this cart item has options. -->
						@if (Cartify::cart($cart_name)->has_options($item['rowid']))
						<small>
							<ul class="unstyled">
							@foreach (Cartify::cart($cart_name)->item_options($item['rowid']) as $option_name => $option_value)
								<li>- <small>{{ $option_name }}: {{ array_get($product_options, $option_name . '.' . $option_value) }}</small></li>
							@endforeach
							</ul>
						</small>
						@endif
					</td>
					<td><input type="text" class="span1" value="{{ $item['qty'] }}" name="items[{{ $item['rowid'] }}]" style="text-align:right;"/></td>
					<td><h6><span class="text-danger">&yen;{{ format_number($item['price']) }}</span></h6></td>
					<td><h6><span class="text-danger">&yen;{{ format_number($item['subtotal']) }}</span></h6></td>
				</tr>
				@empty
				<tr>
					<td colspan="6">蛋购袋里啥都没有了,赶快去SHOPPING.</td>
				</tr>
				@endforelse
			</tbody>
		</table>

		@if (Cartify::cart($cart_name)->total())

		<div class="row-fluid">
			<button type="submit" id="update" name="update" value="1" class="btn btn-success btn-embossed btn-embossed pull-right mas">更新蛋购袋</button>
			<button type="submit" id="empty" name="empty" value="1" class="btn btn-embossed pull-right mas">全都不要了</button>
		</div>

		<div class="form-actions">
            <span class="pull-right">
                <span style="font-size: 1.7em;"><b>总价: <span class="text-danger">&yen;{{ format_number(Cartify::cart($cart_name)->total()) }}</span></b></span>
				<a href="/deliveryconfirm" class="btn btn-danger btn-embossed mas">确认订单</a>
            </span>
        </div>

		<div class="row-fluid">

		</div>

		@endif
	</form>
</div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
@endsection
