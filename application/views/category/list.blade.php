@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')

<div class="container-narrow">

    <div class="row-fluid">
        <div class="breadcrumb-text">
            <span class="caption"><i class="icon-tags"></i>蛋壳东西</span>
            <p>
                <span class="text-primary" style="font-size:17px;">{{ Session::get('cateName') }}</span>
            </p>
        </div>
        <div class="demo-tiles">
            <div class="thumbnails">
                @foreach($cates->results as $category)
                    <div class="demo-col">
                        <div class="tile mbl">
                            <a href="/store-products/{{ $category->id }}">
                                {{ HTML::image('img'.$category->category_img, $category->category_name, array('class' => 'tile-image')) }}
                            </a>

                            <a class="btn btn-info btn-embossed" href="/store-products/{{ $category->id }}" style="width:80%;">
                                {{ $category->category_name }}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="text-center">{{ $cates->links(); }}</div>
    </div>

</div>
@endsection

@section('footer')
    @parent
@endsection

@section('footerScript')
    @parent
@endsection
