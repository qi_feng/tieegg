<!DOCTYPE HTML>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="description" content="大连母婴用品在线购物，父母所需要的也是我们专注的, 等待我们为您的宝宝精心准备的惊喜！宝宝用品,婴童用品,新生儿用品,孕妇婴儿用品,母婴,母婴用品,孕婴用品,儿童用品">
    <meta name="Keywords" content="宝宝用品, 婴童用品, 新生儿用品, 孕妇婴儿用品, 母婴, 母婴用品, 孕婴用品, 儿童用品, 大连网上购物, 大连网购, 大连铁蛋壳">
    <meta name="buildInfo" content="tieegg-1.0.0"/>
    <meta name="author" content="Feng Qi"/>
    <meta property="qc:admins" content="321301331764155776375"/>
    <meta property="wb:webmaster" content="7f7548acd17fd73e"/>
    <title>{{ $title }} 铁蛋壳 : 生活始于宝贝的世界.</title>
    <link href="{{ URL::to('favicon.ico') }}" rel="shortcut icon"/>
    <script type="text/javascript" src="http://qzonestyle.gtimg.cn/qzone/openapi/qc_loader.js" data-appid="100427975" data-redirecturi="http://www.tieegg.com/qqSignin" charset="utf-8"></script>
    @section('headerScript')
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/bootstrap-responsive.css') }}
        {{ HTML::style('css/flat-ui.css') }}
        {{ HTML::style('css/sub.css') }}
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
            {{ HTML::script('js/html5shiv.js') }}
        <![endif]-->
    @yield_section
    {{ HTML::script('js/jquery-1.8.3.min.js') }}
    {{ HTML::script('js/modernizr-2.6.2.min.js') }}
    {{ HTML::style('plugins/fontAwesome/css/font-awesome.min.css') }}
    <!--[if IE 7]>
    {{ HTML::style('plugins/fontAwesome/css/font-awesome-ie7.min.css') }}
    <![endif]-->
</head>
<body>
    <header>
    @section('header')
    <div class="container">
        <div class="demo-content-wide">
            <div class="navbar navbar-inverse" style="margin-top: 10px;">
                <div class="navbar-inner">
                    <div class="container">
                        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target="#nav-top-link"></button>
                        <a class="brand" href="/">
                            {{ HTML::image('/img/tieegg-logo.png', '铁蛋壳', array('style' => 'width:7em;')) }}
                        </a>
                        <div class="nav-collapse collapse" id="nav-top-link">
                            <ul class="nav">
                                <li>
                                    <a href="/"><i class="icon-home"></i>主页</a>
                                </li>
                            </ul>
                            <ul class="nav">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-tags"></i>蛋壳东西
                                    </a>
                                    <ul>
                                        <li><a href="/store-categorys/usage"><i class="icon-sitemap"></i>类别SHOPPING</a></li>
                                        <li><a href="/store-categorys/age"><i class="icon-calendar"></i>月龄SHOPPING</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav">
                                <li>
                                    <a href="/cart"><i class="icon-shopping-cart"></i>蛋购袋</a>
                                </li>
                            </ul>
                            <!-- <ul class="nav">
                                <li>
                                    <a href="#"><i class="icon-heart"></i>心愿单</a>
                                </li>
                            </ul> -->
                            <!-- <form class="navbar-search form-search pull-left" action="" align="center">
                                <div class="input-append">
                                    <input type="text" class="search-query span2" placeholder="Search">
                                    <button type="submit" class="btn btn-large">
                                        <i class="fui-search"></i>
                                    </button>
                                </div>
                            </form> -->
                            <ul class="nav pull-right">
                                @if (Sentry::check())
                                    <li>
                                        <a href="{{ URL::to('userinfo') }}"><i class="icon-user"></i>我的蛋壳</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ URL::to('signin') }}"><i class="icon-signin"></i>登入</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield_section
    </header>

    <div>
        @yield('content')
    </div>

    <footer>
        @section('footer')
            <div class="bottom-menu bottom-menu-large">
                <div class="container">
                    <div class="row">
                        <div class="span2 brand">
                            <a href="#fakelink" class="fui-flat"></a>
                        </div>

                        <div class="span2">
                            <h5 class="title">关于铁蛋壳</h5>
                        </div>

                        <div class="span2">
                            <h5 class="title">类别SHOPPING</h5>
                        </div>

                        <div class="span2">
                            <h5 class="title">月龄SHOPPING</h5>
                        </div>

                        <div class="span2">
                            <h5 class="title">我的蛋壳</h5>
                        </div>

                    </div>
                </div>
            </div>
        @yield_section
    </footer>

    @section('footerScript')
    <!-- Load JS here for greater good =============================-->
    {{ HTML::script('js/jquery-ui-1.10.3.custom.min.js') }}
    {{ HTML::script('js/jquery.ui.touch-punch.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/bootstrap-select.js') }}
    {{ HTML::script('js/bootstrap-switch.js') }}
    {{ HTML::script('js/flatui-checkbox.js') }}
    {{ HTML::script('js/flatui-radio.js') }}
    {{ HTML::script('js/jquery.tagsinput.js') }}
    {{ HTML::script('js/jquery.placeholder.js') }}
    {{ HTML::script('js/jquery.stacktable.js') }}
    {{ HTML::script('js/application.js') }}
    {{ HTML::script('plugins/parsley/parsley.js') }}

    <!-- JAVASCRIPT初始化 START -->
    <script type="text/javascript">
    </script>
    <!-- JAVASCRIPT初始化 END -->
    @yield_section

    <!-- google analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-40493157-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</body>
</html>
