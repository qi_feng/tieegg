@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
<div class="container-narrow">

    <div class="row-fluid">
        <div class="breadcrumb-text">
            <span class="caption"><i class="icon-tags"></i>蛋壳东西</span>
            <p>
                <a href="/store-categorys/{{ Session::get('cateLink') }}" class="text-primary"><span style="font-size: 17px;">{{ Session::get('cateName') }}</span></a>
                <span class="text-success" style="font-size: 17px;">{{ Session::get('subCateName') }}</span>
            </p>
        </div>
        <div class="demo-tiles">
            <div class="thumbnails">
                @forelse($products->results as $product)
                    <div class="demo-col">
                        @if ($product->pop_flg == 1)
                        <div class="tile tile-hot mbl">
                        @else
                        <div class="tile mbl">
                        @endif
                            <a href="/show-product/{{ $product->id }}" target="_blank" class="thumbnail mtl mbl">
                                {{ HTML::image('img'.$product->product_img_url, $product->product_name) }}
                            </a>
                            <div class="tile-title" style="font-size: 1.2em;height:53px;"><b><a href="/show-product/{{ $product->id }}">{{ $product->product_name }}</a></b></div>
                            <div style="margin-top: 10px;">蛋壳价：<span class="label label-important"><b>&yen;{{ $product->coupon_price }}</b></span></div>
                            <a href="/show-product/{{ $product->id }}"><h4></h4></a>
                            @if ($product->pop_flg == 1)
                            <button id="addCartBtn{{$product->id}}" class="btn btn-danger btn-embossed" style="width: 90%;">
                            @else
                            <button id="addCartBtn{{$product->id}}" class="btn btn-info btn-embossed" style="width: 90%;">
                            @endif
                                加入蛋购袋
                            </button>
                        </div>
                    </div>
                @empty
                    <h2>没找到什么,看看别的吧!</h2>
                @endforelse
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 text-center">{{ $products->links(); }}</div>
    </div>

</div>
@endsection

@section('footer')
    @parent
@endsection

@section('footerScript')
    @parent
    <script type="text/javascript">
        $(function(){
            $('button[id^="addCartBtn"]').click(function(){
                var item_id = this.id.substr(10);
                $.ajax({
                    type: "POST",
                    url: "/add-cart",
                    datatype: 'json',
                    async: true,
                    data: {
                        'item_id': item_id,
                        'qty': 1,
                        'action': 'add_to_cart'
                    },
                    success: function(msg){
                        if (!msg.error) {
                            $('button[id="addCartBtn' + item_id + '"]').html('货品已加入!');
                            $('button[id="addCartBtn' + item_id + '"]').attr('disabled', 'disabled');
                            window.setTimeout(function(){
                                $('button[id="addCartBtn' + item_id + '"]').html('加入蛋购袋');
                                $('button[id="addCartBtn' + item_id + '"]').removeAttr('disabled');
                            }, 1000);
                        } else {
                            alert(msg.message);
                        }
                    },
                    error: function(e){
                        //alert(JSON.stringify(e));
                    }
                });
            });
        });
    </script>
@endsection
