@layout('base')

@section('headerScript')
    @parent
    <script type="text/javascript">
        var uyan_config = {
             'title':'{{ $product->product_name }}',
             'url':'',
             'pic':'{{ URL::to_asset('img'.$product->product_img_url) }}',
             'vid':'',
             'du':'',
             'su':''
        };
    </script>

    {{ HTML::style('plugins/slider/jquery.bxslider.css') }}
@endsection

@section('header')
    @parent
@endsection

@section('content')
<div class="container-narrow">
    <div class="breadcrumb-text">
        <span class="caption"><i class="icon-tags"></i>蛋壳东西</span>
        <p>
            <a href="/store-categorys/{{ Session::get('cateLink') }}" class="text-primary"><span style="font-size:17px;">{{ Session::get('cateName') }}</span></a>
            <a href="/store-products/{{ Session::get('subCateLink') }}" class="text-success"><span style="font-size:17px;">{{ Session::get('subCateName') }}</span></a>
            <!-- <a href="/store-categorys/{{ $brand }}" class="text-warning">{{ $brand }}</a> -->
            <span class="text-warning" style="font-size:17px;">{{ $brand }}</span>
            <input type="hidden" name="item_id" id="item_id" value="{{ $product->id}}" />
        </p>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h4 class="product-detail-title">
                <b><i class="icon-tag"></i>{{ $product->product_name }}</b>
            </h4>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
            <div class="span12">
                <ul class="bxslider" style="margin:0px;">
                    <li>{{ HTML::image('img'.$product->product_img_url, $product->product_name) }}</li>
                    @foreach ($productImgs as $img)
                        <li>{{ HTML::image('img'.$img->img_url, $product->product_name) }}</li>
                    @endforeach
                </ul>
                <div id="bx-pager">
                    <a data-slide-index="0" href="">{{ HTML::image('img'.$product->product_img_url, $product->product_name, array('class' => 'img-polaroid img-rounded', 'style' => 'width:20%;')) }}</a>

                    @for ($i = 1; $i <= sizeOf($productImgs); $i++)
                        <a data-slide-index="{{ $i }}" href="">{{ HTML::image('img'.$productImgs[$i-1]->img_url, $product->product_name, array('class' => 'img-polaroid img-rounded', 'style' => 'width:20%;')) }}</a>
                    @endfor
                </div>
            </div>
        </div>
        <div class="span4 mtm">
            <div class="row-fluid">
                <div class="span12">
                    <div class="demo-popovers">
                        <div class="popover right">
                            <div class="arrow"></div>
                            <h3 class="popover-title">
                                <i class="icon-money"></i>
                                <span class="">{{ $brand }}</span> <i class="icon-double-angle-right"></i> {{ $product->product_name }}
                            </h3>
                            <div class="popover-content">
                                <h6>
                                    <b>蛋壳价</b>
                                    <span class="text-danger mrs mls"><b>&yen;{{ $product->coupon_price }}</b></span>
                                    <br>
                                    <span style="text-decoration:line-through; color:gray;"><b>市场价</b></span>
                                    <span class="mrs mls" style="text-decoration:line-through; color:gray;">&yen;{{ $product->my_price }}</span>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12 prm">
                    <button id="addCartBtn" class="btn btn-danger btn-block btn-embossed"><i class="icon-ok-sign mrs"></i>加入蛋购袋</button>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12 prm">
                    <div class="product_notebook">
                        <section>
                            <div class="headfont">product information</div>
                            <!-- <div class="mas">
                                <b>还有多少</b>
                                <span class="label label-info">{{ $product->stock_count }}</span>
                            </div> -->
                            <div class="mas">
                                <b>卖出多少</b>
                                <span class="label label-info">{{ $product->sold_count }}</span>
                            </div>
                        </section>
                        <section>
                            <div class="headfont">shop information</div>
                            <!-- <div class="mas">
                                <b>尺寸</b><br><br>
                                {{ Form::select('sizeId', $size, null, array('id' => 'sizeId', 'class' => 'small select-block')) }}
                            </div> -->
                            <div class="mas control-group">
                                <b>数量</b><br><br>
                                <input type="text" id="spinner-01" class="spinner" placeholder="" value="1" disabled>
                            </div>
                        </section>
                        <section>
                            <div class="headfont">let's share</div>
                            <!-- JiaThis 社会化分享插件 BEGIN -->
                            <div class="jiathis_style mas">
                                <a class="jiathis_button_qzone"></a>
                                <a class="jiathis_button_tsina"></a>
                                <a class="jiathis_button_tqq"></a>
                                <a class="jiathis_button_weixin"></a>
                                <a class="jiathis_button_renren"></a>
                                <a href="http://www.jiathis.com/share?uid=1764777" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
                                <a class="jiathis_counter_style"></a>
                            </div>
                            <!-- JiaThis 社会化分享插件 END -->
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row-fluid">
        <div class="detail-hr"></div>
    </div> -->
    <div class="row-fluid">
        <div class="span12">

            <div class="content-padding-top" id="basicInfo">
                <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                    <div class="pull-left" style="margin-left:10px;"><h4>基本信息</h4></div>
                </div>
                <div class="mal">
                    <table class="table table-bordered table-hover table-striped">
                        <thead style="background-color:#E5E5E5;">
                            <th>品牌 Brand</th>
                            <th>尺寸 Size</th>
                            <th>重量 Weight</th>
                            <th>材质 Material</th>
                            <th>产地 Origin</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $brand }}</td>
                                <td>53 x 3.5 x 6.8 cm</td>
                                <td>6 Kg</td>
                                <td>PPSU</td>
                                <td>中国 广东</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="content-padding-top" id="describeInfo">
                <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                    <div class="pull-left" style="margin-left:10px;"><h4>货品描述</h4></div>
                </div>
                <div class="mal">
                    @foreach ($productInfos as $info)
                        <img src="{{ URL::to_asset('img' . $info->info_img_url) }}" />
                    @endforeach
                </div>
                <div class="mal">
                    {{ $product->product_detail }}
                </div>
            </div>

            <div class="content-padding-top" id="comments">
                <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                    <div class="pull-left" style="margin-left:10px;"><h4>蛋友咿呀</h4></div>
                </div>
                <!-- UY BEGIN -->
                <div id="uyan_frame" class="mal"></div>
                <!-- UY END -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent

    <!-- jiathis插件JS引用 START -->
    <script type="text/javascript" >
        var jiathis_config={
            data_track_clickback:true,
            url:"{{ URL::full() }}",
            summary:"铁蛋壳 货品名称: {{ $product->product_name }}",
            title:"铁蛋壳 货品名称: {{ $product->product_name }} #铁蛋壳#",
            pic:"{{ URL::to_asset('img'.$product->product_img_url) }}",
            ralateuid:{
                "tsina":"fayvon1984"
            },
            hideMore:false
        }
    </script>
    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1764777" charset="utf-8"></script>
    <script type="text/javascript" id="UYScript" src="http://v1.uyan.cc/js/iframe.js?UYUserId=1764777" async=""></script>
    <!-- jiathis插件JS引用 END -->

    <!-- bxSlider Javascript file -->
    {{ HTML::script('plugins/slider/jquery.bxslider.min.js') }}

    <!-- 自定义JS初始化 START -->
    <script type="text/javascript">
        $(function(){
            //$('body').addClass("detail-back-img");
            $('.bxslider').bxSlider({
                pagerCustom: '#bx-pager'
            });

            $('#addCartBtn').click(function(){
                $.ajax({
                    type: "POST",
                    url: "/add-cart",
                    datatype: 'json',
                    async: true,
                    data: {
                        'item_id': $('#item_id').val(),
                        'qty': $('#spinner-01').val(),
                        'action': 'add_to_cart'
                    },
                    success: function(msg){
                        if (!msg.error) {
                            $('#addCartBtn').html('货品已加入!');
                            $('#addCartBtn').attr('disabled', 'disabled');
                            window.setTimeout(function(){
                                $('#addCartBtn').html('加入蛋购袋');
                                $('#addCartBtn').removeAttr('disabled');
                            }, 1000);
                        } else {
                            alert(msg.message);
                        }
                    },
                    error: function(e){
                    }
                });
            });

        });

    </script>
    <!-- 自定义JS初始化 END -->

@endsection
