@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')

    <br>
    <br>
    {{ Form::open_for_files('add-product') }}

        @if (Session::has('error_flg'))
        <div class="row">
            <div class="one third padded"></div>
            <div class="two thirds padded"><p class="warning message">{{ Session::get('error_msg') }}</p></div>
        </div>
        @endif

        <div class="row">
            <div class="two centered mobile thirds">
                <fieldset>
                <div class="row">
                    <div class="one whole padded">
                        <label for="productName">货品名称</label>
                        {{ Form::text('productName', null, array('id' => 'productName')) }}
                    </div>
                </div>

                <div class="row">
                    <div class="one half padded">
                        <label for="productImageUrl">货品图片</label>
                        {{ Form::file('productImageUrl', array('id' => 'productImageUrl')) }}
                    </div>
                    <div class="one half padded">
                        <label for="categoryId">分类</label>
                        <span class="select">
                            {{ Form::select('categoryId', $categoryAry, null, array('id' => 'categoryId')) }}
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="one fourth padded">
                        <label for="myPrice">蛋价</label>
                        <div class="row">
                            <div class="one third"><span class="prefix">$</span></div>
                            <div class="two thirds"><input type="text" id="myPrice" name="myPrice" /></div>
                        </div>
                    </div>
                    <div class="one fourth padded">
                        <label for="couponPrice">折扣价</label>
                        <div class="row">
                            <div class="one third"><span class="prefix">$</span></div>
                            <div class="two thirds"><input type="text" id="couponPrice" name="couponPrice" /></div>
                        </div>
                    </div>
                    <div class="one fourth padded">
                        <label for="discount">折扣</label>
                        <div class="row">
                            <div class="one third"><span class="prefix">%</span></div>
                            <div class="two thirds">
                                <span class="select">
                                    {{ Form::select('discount', $discountAry, null, array('id' => 'discount')) }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="one fourth padded">
                        <label for="active">上架状态</label>
                        <input type="text" id="active" name="active" />
                    </div>
                </div>

                <div class="row">
                    <div class="one third padded">
                        <label for="stockCount">在库数量</label>
                        <input type="text" id="stockCount" name="stockCount" />
                    </div>
                    <div class="one third padded">
                        <label for="stockCount">售出数量</label>
                        <input type="text" id="stockCount" name="stockCount" />
                    </div>
                    <div class="one third padded">
                        <label for="sort">上架顺序</label>
                        <input type="text" id="sort" name="sort" />
                    </div>
                </div>

                <div class="row">
                    <div class="one padded">
                        <label for="productDetail">货品描述</label>
                        <textarea id="productDetail"></textarea>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>

        <div class="row">
            <div class="two eighths padded"></div>
            <div class="four eighth align-center padded">
                {{ Form::submit('追加', array('id' => 'addBtn')) }}
                {{ Form::reset('重置', array('class' => 'warning')) }}
            </div>
            <div class="two eighths padded"></div>
        </div>

    {{ Form::close() }}

@endsection

@section("footer")
    @parent
@endsection

@section("footerScript")
    @parent
@endsection
