@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
<br><br><br><br><br><br><br>
    <div class="row">
        <div class="one third mobile skip-one">
            <table>
                <thead>
                    <tr>
                        <th>分类管理</th>
                        <th>货品管理</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="#">分类追加</a></td>
                        <td><a href="add-product">货品追加</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('footerScript')
    @parent
@endsection
