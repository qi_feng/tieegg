@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
@endsection
