@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        <fieldset>
            <div class="row-fluid" style="margin-bottom: 0px;">
                <div class="span12 pull-left muted">
                    <h4><b>我的订单</b></h4>
                </div>
            </div>
            <div class="row-fluid">

            </div>
        </fieldset>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        $(function(){

        });
    </script>
@endsection
