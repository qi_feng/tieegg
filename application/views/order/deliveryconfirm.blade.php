@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        <fieldset>

            <div class="row-fluid" style="margin-bottom: 0px;">
                <div class="span12 pull-left muted">
                    <h4><b>STEP1 . 填写收货地址</b></h4>
                </div>
            </div>

            @if (Session::has('error_flg'))
            <div class="control-group">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span class="text-primary">{{ Session::get('error_msg') }}</span>
                </div>
            </div>
            @endif
            @if (Session::has('success_flg'))
            <div class="control-group">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('success_msg') }}
                </div>
            </div>
            @endif

            <div class="row-fluid">
                <div class="well">
                    {{ Form::open('deliveryconfirm', 'POST', array('class' => 'form-horizontal', 'data-validate' => 'parsley')) }}
                        <div class="control-group {{ $errors->has('guestname') ? 'error' : '' }}" style="margin-top:20px;">
                            {{ Form::label('guestname', '收货人', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('guestname', Input::old('guestname'), array('class' => 'login-field input-large', 'placeholder' => '收货人', 'id' => 'guestname', 'data-trigger' => 'keyup', 'data-error-container' => '#guestnameErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                <span id="guestnameErrSpan" class="help-inline">
                                    @if ($errors->has('guestname'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('guestname') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('province') ? 'error' : '' }}">
                            {{ Form::label('province', '省', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('province', Input::old('province'), array('class' => 'login-field input-large', 'placeholder' => '例:辽宁省', 'id' => 'province', 'data-trigger' => 'keyup', 'data-error-container' => '#provinceErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                <span id="provinceErrSpan" class="help-inline">
                                    @if ($errors->has('province'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('province') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('city') ? 'error' : '' }}">
                            {{ Form::label('city', '市', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('city', Input::old('city'), array('class' => 'login-field input-large', 'placeholder' => '例:大连市', 'id' => 'city', 'data-trigger' => 'keyup', 'data-error-container' => '#cityErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                <span id="cityErrSpan" class="help-inline">
                                    @if ($errors->has('city'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('city') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('area') ? 'error' : '' }}">
                            {{ Form::label('area', '区,县', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('area', Input::old('area'), array('class' => 'login-field input-large', 'placeholder' => '例:甘井子区', 'id' => 'area', 'data-trigger' => 'keyup', 'data-error-container' => '#areaErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,20]')) }}
                                <span id="areaErrSpan" class="help-inline">
                                    @if ($errors->has('area'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('area') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('address') ? 'error' : '' }}">
                            {{ Form::label('address', '街道,小区,门牌', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('address', Input::old('address'), array('class' => 'login-field input-large span8', 'placeholder' => '例:**街*号****小区**栋**楼**号', 'style' => 'height: 41px;', 'id' => 'address', 'data-trigger' => 'keyup', 'data-error-container' => '#addressErrSpan', 'data-required' => 'true', 'data-rangelength' => '[1,100]')) }}
                                <span id="addressErrSpan" class="help-inline">
                                    @if ($errors->has('address'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('address') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('postcode') ? 'error' : '' }}">
                            {{ Form::label('postcode', '邮政编码', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('postcode', Input::old('postcode'), array('class' => 'login-field input-large', 'placeholder' => '例:116023', 'id' => 'postcode', 'data-trigger' => 'keyup', 'data-error-container' => '#postcodeErrSpan', 'data-required' => 'true', 'data-minlength' => '6', 'data-maxlength' => '6')) }}
                                <span id="postcodeErrSpan" class="help-inline">
                                    @if ($errors->has('postcode'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('postcode') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="control-group {{ $errors->has('contactphone') ? 'error' : '' }}">
                            {{ Form::label('contactphone', '联系电话', array('class' => 'control-label')) }}
                            <div class="controls">
                                {{ Form::text('contactphone', Input::old('contactphone'), array('class' => 'login-field input-large span6', 'style' => 'height: 41px;', 'placeholder' => '例:手机:13112341234/座机:0411-12345678', 'id' => 'contactphone', 'data-trigger' => 'keyup', 'data-error-container' => '#contactphoneErrSpan', 'data-required' => 'true', 'data-minlength' => '7', 'data-maxlength' => '16')) }}
                                <span id="contactphoneErrSpan" class="help-inline">
                                    @if ($errors->has('contactphone'))
                                    <div class="alert alert-error" style="margin:auto;">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $errors->first('contactphone') }}
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="form-actions pbs">
                            <div class="span9">
                                {{ Form::label('addrSaveChk', '保存该地址', array('class' => 'span2 muted mtm mls', 'for' => 'addrCheck')) }}
                                <span class="switch switch-square mas"
                                  data-on-label="<i class='fui-check'></i>"
                                  data-off-label="<i class='fui-cross'></i>">
                                  <input id="addrCheck" name="addrCheck" type="checkbox" value="1" />
                                </span>
                            </div>
                            <div class="span3">
                                {{ Form::submit('确认并继续', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-block btn-embossed')) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>

            <div class="row-fluid" style="margin-bottom: 0px;">
                <div class="span12 pull-left muted">
                    <h6><b>选择既存地址</b></h6>
                </div>
            </div>

            <div class="row-fluid">
                @if (count($deliveryAddrs) > 0)
                    <table class="table table-bordered table-hover table-striped">
                        <thead style="background-color:#ECF0F1;">
                            <th>收货人</th>
                            <th>收货地址</th>
                            <th>联系电话</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @for ($i = 0; $i < count($deliveryAddrs); $i++)
                            <tr>
                                <td>{{ $deliveryAddrs[$i]->name }}</td>
                                <td>{{ $deliveryAddrs[$i]->province }} {{ $deliveryAddrs[$i]->city }} {{ $deliveryAddrs[$i]->area }} {{ $deliveryAddrs[$i]->address }} {{ $deliveryAddrs[$i]->post_code }}</td>
                                <td>{{ $deliveryAddrs[$i]->contact_phone }}</td>
                            @if ($deliveryAddrs[$i]->main_addr == 1)
                                <td onclick="addrRadioBtnClick({{ $i + 1 }});" style="padding-top:2px;padding-bottom:2px;"><button class="btn btn-mini btn-embossed btn-success" name="chooseBtn{{ $i + 1 }}">选择</button></td>
                            @else
                                <td onclick="addrRadioBtnClick({{ $i + 1 }});" style="padding-top:2px;padding-bottom:2px;"><button class="btn btn-mini btn-embossed" name="chooseBtn{{ $i + 1 }}">选择</button></td>
                            @endif
                            </tr>
                            <input type="hidden" id="name{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->name }}"/>
                            <input type="hidden" id="province{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->province }}"/>
                            <input type="hidden" id="city{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->city }}"/>
                            <input type="hidden" id="area{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->area }}"/>
                            <input type="hidden" id="address{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->address }}"/>
                            <input type="hidden" id="contact_phone{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->contact_phone }}"/>
                            <input type="hidden" id="post_code{{ $i + 1 }}" value="{{ $deliveryAddrs[$i]->post_code }}"/>

                        @endfor
                        </tbody>
                    </table>
                    <input type="hidden" id="mainAddrNum" value="{{ $mainAddrNum }}"/>
                @else
                    <div class="alert"><strong>嘿!</strong> 你还没有添加送货地址哦!</div>
                @endif
            </div>

        </fieldset>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        $(function(){
            var mainAddrNum = $('#mainAddrNum').val();
            $('#guestname').val($('#name' + mainAddrNum).val());
            $('#province').val($('#province' + mainAddrNum).val());
            $('#city').val($('#city' + mainAddrNum).val());
            $('#area').val($('#area' + mainAddrNum).val());
            $('#address').val($('#address' + mainAddrNum).val());
            $('#contactphone').val($('#contact_phone' + mainAddrNum).val());
            $('#postcode').val($('#post_code' + mainAddrNum).val());

            $('#')
        });

        /**
         * 地址选择radio按钮点击事件
         */
        function addrRadioBtnClick(rowid) {
            $('#guestname').val($('#name' + rowid).val());
            $('#province').val($('#province' + rowid).val());
            $('#city').val($('#city' + rowid).val());
            $('#area').val($('#area' + rowid).val());
            $('#address').val($('#address' + rowid).val());
            $('#contactphone').val($('#contact_phone' + rowid).val());
            $('#postcode').val($('#post_code' + rowid).val());

            $('button[name^="chooseBtn"]').each(function(){
                $(this).removeClass('btn-success');
            });

            $('button[name^="chooseBtn' + rowid + '"]').each(function(){
                $(this).toggleClass('btn-success');
            });
        }
    </script>
@endsection
