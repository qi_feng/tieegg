@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        <fieldset>
            <div class="row-fluid" style="margin-bottom: 0px;">
                <div class="span12 pull-left muted">
                    <h4><b>STEP2 . 选择支付方式</b></h4>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="tile mal">
                        {{ HTML::image('images/icons/Card@2x.png', '在线支付', array('class' => 'tile-image-pay')) }}
                        <a class="btn btn-danger btn-block btn-embossed" href="/orderconfirm/payonline">在线支付</a>
                    </div>
                </div>
                <div class="span6">
                    <div class="tile mal">
                        {{ HTML::image('images/icons/Money@2x.png', '货到付款', array('class' => 'tile-image-pay')) }}
                        <a class="btn btn-success btn-block btn-embossed" href="/orderconfirm/payoffline">货到付款</a>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        $(function(){

        });
    </script>
@endsection
