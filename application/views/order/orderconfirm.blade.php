@layout('base')

@section('headerScript')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('content')
    <div class="container-narrow">
        <fieldset>
            <div class="row-fluid" style="margin-bottom: 0px;">
                <div class="span12 pull-left muted">
                    <h4><b>STEP3 . 确认订单内容</b></h4>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span10 offset1">
                    <div class="content-padding-top" id="basicInfo">
                        <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                            <div class="pull-left" style="margin-left:10px;"><h6>1 . 货品清单</h6></div>
                        </div>
                        <div class="mal">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th width="40%">货品名</th>
                                        <th width="10%">数量</th>
                                        <th width="12%">单价</th>
                                        <th width="12%">共计</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($cart_contents as $item)
                                    <!-- Get the product options, you should get product related options on your controller! -->
                                    <?php $product_options = Cartify\Models\Products::get_options($item['id']); ?>

                                    <tr>
                                        <td>
                                            <strong><a href="{{ URL::to('show-product/' . $item['id']) }}" target="_blank">{{ $item['name'] }}</a></strong>

                                            <!-- Check if this cart item has options. -->
                                            @if (Cartify::cart($cart_name)->has_options($item['rowid']))
                                            <small>
                                                <ul class="unstyled">
                                                @foreach (Cartify::cart($cart_name)->item_options($item['rowid']) as $option_name => $option_value)
                                                    <li>- <small>{{ $option_name }}: {{ array_get($product_options, $option_name . '.' . $option_value) }}</small></li>
                                                @endforeach
                                                </ul>
                                            </small>
                                            @endif
                                        </td>
                                        <td>{{ $item['qty'] }}</td>
                                        <td><h6><span class="text-danger">&yen;{{ format_number($item['price']) }}</span></h6></td>
                                        <td><h6><span class="text-danger">&yen;{{ format_number($item['subtotal']) }}</span></h6></td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6">没有货品被加入购物车,赶快去SHOPPING.</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                        <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                            <div class="pull-left" style="margin-left:10px;"><h6>2 . 配送信息</h6></div>
                        </div>
                        <?php $deliveryAddrs = Session::get('orderAddr') ?>
                        <div class="mal">
                            <table class="table table-bordered table-hover table-striped">
                                <thead style="background-color:#ECF0F1;">
                                    <th>收货人</th>
                                    <th>收货地址</th>
                                    <th>联系电话</th>
                                </thead>
                                <tbody>
                                    <tr class="info">
                                        <td>{{ $deliveryAddrs['name'] }}</td>
                                        <td>{{ $deliveryAddrs['province'] }} {{ $deliveryAddrs['city'] }} {{ $deliveryAddrs['area'] }} {{ $deliveryAddrs['address'] }} {{ $deliveryAddrs['post_code'] }}</td>
                                        <td>{{ $deliveryAddrs['contact_phone'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="dialog" style="margin-top: 20px;padding-top:5px;padding-bottom: 40px;">
                            <div class="pull-left" style="margin-left:10px;"><h6>3 . 支付方式</h6></div>
                        </div>
                        <div class="mal">
                            @if ($paytype == 'payonline')
                                支付宝在线支付
                            @else
                                货到付款
                            @endif
                        </div>

                        {{ Form::open('orderconfirm/'.$paytype, 'POST') }}
                        <div class="form-actions">
                            <span class="pull-right span6">
                                <span style="font-size: 1.7em;" class="mrl"><b>总价: <span class="text-danger">&yen;{{ format_number(Cartify::cart($cart_name)->total()) }}</span></b></span>
                                @if ($paytype == 'payonline')
                                {{ Form::submit('提交并支付订单', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-embossed')) }}
                                @else
                                {{ Form::submit('提交订单', array('id' => 'signupBtn', 'class' => 'btn btn-danger btn-embossed')) }}
                                @endif
                            </span>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section("footerScript")
    @parent
    <script type="text/javascript">
        $(function(){

        });
    </script>
@endsection
