<?php

class Create_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
			// auto incremental id (PK)
			$table->increments('id');

			// varchar 32
			$table->string('username', 32);
			$table->string('nickname', 128);
			$table->string('password', 64);
			$table->string('email', 320);

			// int
			$table->integer('role');

			// boolean
			$table->boolean('active');

			// create_at | udpated_at DATETIME
			$table->timestamps();
		});

		DB::table('users')->insert(array(
			'username' => 'admin',
			'nickname' => 'Admin',
			'email'    => 'admin@tieegg.com',
			'role'     => 1,
			'active'   => true,
			'password' => Hash::make('password')
		));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::Drop('users');
	}

}
