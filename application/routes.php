<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name." ≥ hn n      ;
|		});
|
*/

/*** 路由配置 START ***/
// 画面:主画面(根URL调用主画面action)
Route::get('/', function()
{
    // 指向"home"控制器@"index"方法
	return Redirect::to_action('home');
});

// 画面:蛋出(closure:无Action模式)
Route::get('/signout', function(){
    // 清空当前用户购物车Session数据
    Cartify::cart('user_cart_' . Sentry::user()->id)->destroy();
    // 删除session
    Sentry::logout();
    // 指向URL "/" (调用上面Redirect::to_action('home');)
    return Redirect::to('/');
});

// 画面:购物车
Route::any('/cart', 'cart@cart');

// 回调画面:支付宝同步通知
Route::get('/alipay_return_url', 'order@alipayreturn');

// 回调画面:支付宝异步通知
Route::post('/alipay_notify_url', 'order@alipaynotify');

/*** 路由配置 END ***/

/*** 转意路由字符 START ***/
// 画面:蛋壳东西
Route::any('/store-categorys/(:any)', 'category@list');
// 画面:货品列表
Route::any('/store-products/(:any)', 'product@list');
// 画面:货品详细
Route::get('/show-product/(:any)', 'product@detail');
// 画面:加入购物车
Route::post('add-cart', 'product@detail');
// 画面:购物车 移除商品
Route::get('/remove-product/(:any)', 'cart@remove');
/*** 转意路由字符 END ***/

/*
|--------------------------------------------------------------------------
| Application controller assign
|--------------------------------------------------------------------------
*/
Route::controller('home'); // 主画面action
Route::controller('account'); // 用户控制action
Route::controller('product'); // 货品控制action
Route::controller('category'); // 分类控制action
Route::controller('cart'); // 购物车action
Route::controller('order'); // 订单action

/*
|--------------------------------------------------------------------------
| Application controller route group setting
|--------------------------------------------------------------------------
*/
/*** 需要--用户--验证的画面路由设置 START ***/
Route::group(array('before' => 'auth'), function(){
    // 画面:蛋友详细初始化
    Route::get('/userinfo', 'account@userinfo');
    // 画面:蛋友详细提交
    Route::post('/userinfo', 'account@userinfo');
    // 画面:密码修改提交
    Route::post('/passchange', 'account@passchange');
    // 画面:送货地址追加提交
    Route::post('/deliveryadd', 'account@deliveryadd');
    // 画面:设置默认送货地址提交
    Route::post('/set_default_addr', 'account@setdefaultaddr');
    // 画面:删除送货地址提交
    Route::post('/remove_addr', 'account@removeaddr');
    // 画面:送货地址修改提交
    Route::post('/deliverychange', 'account@deliverychange');
    // 画面:订单送货地址确认初始化
    Route::get('/deliveryconfirm', 'order@deliveryconfirm');
    // 画面:订单送货地址确认提交
    Route::post('/deliveryconfirm', 'order@deliveryconfirm');
    // 画面:送货方式确认初始化
    Route::any('/paytypechoose', 'order@paytype');
    // 画面:订单详细确认
    Route::any('/orderconfirm/(:any)', 'order@orderconfirm');
    // 画面:我的订单详细
    Route::post('/mytrade', 'account@mytrade');
});
/*** 需要--用户--验证的画面路由设置 END ***/

/*** 需要--用户--未验证的画面路由设置 START ***/
Route::group(array('before' => 'authed'), function(){
    // 画面:蛋入
    Route::any('/signin', 'account@signin');
    // 画面:注册蛋友
    Route::any('/signup', 'account@signup');
    // 画面:新浪微博用户登录
    Route::get('/sinaSignin', 'account@sinaSignin');
    // 画面:新浪微博用户登录
    Route::any('/qqSignin', 'account@qqSignin');
});
/*** 需要--用户--验证的画面路由设置 END ***/

/*** 需要--管理员--验证的画面路由设置 START ***/
Route::group(array('before' => 'admin'), function(){
    // 管理MENU
    Route::any('admin-menu', function(){
        return View::make('admin.menu');
    });
    // 管理员功能:货品追加
    Route::any('add-product', 'product@add');
});
/*** 需要--管理员--验证的画面路由设置 END ***/

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});


Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('authed', function()
{
    if (Sentry::check()) return Redirect::to('userinfo');
});

Route::filter('auth', function()
{
	if (!Sentry::check()) return Redirect::to('signin');
});

Route::filter('admin', function(){
    if (Auth::guest() or Auth::user()->role != 1) return Redirect::to('signin');
});
